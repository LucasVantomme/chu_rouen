-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 10 mars 2021 à 15:05
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.2.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `api_test`
--

-- --------------------------------------------------------

--
-- Structure de la table `cahiers`
--

DROP TABLE IF EXISTS `cahiers`;
CREATE TABLE IF NOT EXISTS `cahiers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_creation` datetime NOT NULL,
  `createur_id` int(11) DEFAULT NULL,
  `date_archive` datetime DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rattachement` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_archived` tinyint(1) NOT NULL DEFAULT 0,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_91EFE3C573A201E5` (`createur_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `cahiers`
--

INSERT INTO `cahiers` (`id`, `date_creation`, `createur_id`, `date_archive`, `type`, `rattachement`, `is_archived`, `nom`) VALUES
(1, '2021-02-03 16:14:43', 1, NULL, 'type2', 'rattachement1', 0, 'nom beau cahier 1'),
(2, '2021-01-28 16:14:43', 2, NULL, 'type2', 'rattachement2', 0, 'nom beau cahier 2'),
(3, '2021-02-03 16:14:43', NULL, NULL, 'type1', 'rattachement1', 0, 'Mon beau cahier 3'),
(4, '2021-02-03 16:14:43', NULL, NULL, 'type6moncahier', 'rattachementmonchjasfglkjdsfg', 0, 'mon beau cahier 6'),
(6, '2021-02-03 16:14:43', NULL, NULL, 'type7moncahier', 'rattachementmonchjasfglkjdsfg', 0, 'mon beau cahier 7');

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
CREATE TABLE IF NOT EXISTS `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20210204135143', '2021-02-08 15:03:01', 1601),
('DoctrineMigrations\\Version20210204142455', '2021-02-08 15:03:03', 1202),
('DoctrineMigrations\\Version20210204143153', '2021-02-08 15:03:04', 1276),
('DoctrineMigrations\\Version20210204143615', '2021-02-08 15:03:05', 1251),
('DoctrineMigrations\\Version20210204143804', '2021-02-08 15:03:07', 1501),
('DoctrineMigrations\\Version20210204144100', '2021-02-08 15:03:08', 1501),
('DoctrineMigrations\\Version20210204144217', '2021-02-08 15:03:10', 1763),
('DoctrineMigrations\\Version20210204145725', '2021-02-08 15:03:12', 1415),
('DoctrineMigrations\\Version20210204151354', '2021-02-08 15:03:14', 1895),
('DoctrineMigrations\\Version20210205154229', '2021-02-08 15:03:16', 617),
('DoctrineMigrations\\Version20210206152202', '2021-02-08 15:03:17', 228),
('DoctrineMigrations\\Version20210207171113', '2021-02-08 15:03:17', 1192);

-- --------------------------------------------------------

--
-- Structure de la table `fiche_npatient`
--

DROP TABLE IF EXISTS `fiche_npatient`;
CREATE TABLE IF NOT EXISTS `fiche_npatient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_creation` datetime NOT NULL,
  `date_archive` datetime DEFAULT NULL,
  `secteur` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `criticite` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `texte_libre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `conclusion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_archived` tinyint(1) NOT NULL,
  `createur_id` int(11) NOT NULL,
  `cahier_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_41EB5FE473A201E5` (`createur_id`),
  KEY `IDX_41EB5FE41E0D9F2` (`cahier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `fiche_npatient`
--

INSERT INTO `fiche_npatient` (`id`, `titre`, `date_creation`, `date_archive`, `secteur`, `criticite`, `type`, `texte_libre`, `description`, `conclusion`, `is_archived`, `createur_id`, `cahier_id`) VALUES
(1, 'fiche n patient numéro 1', '2021-02-03 16:16:14', NULL, 'biologie', 'max frr', 'type1', 'lorem ipsum', 'lorem ipsum', 'lorem ipsum', 0, 1, 1),
(2, 'fiche n patient numéro 2', '2021-02-01 16:16:14', NULL, 'neurologie', 'tranquille', 'type2', 'lorem ipsum', 'lorem ipsum', 'lorem ipsum', 0, 2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `fiche_patient`
--

DROP TABLE IF EXISTS `fiche_patient`;
CREATE TABLE IF NOT EXISTS `fiche_patient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cahier_id` int(11) NOT NULL,
  `date_creation` datetime NOT NULL,
  `createur_id` int(11) NOT NULL,
  `date_archive` datetime DEFAULT NULL,
  `secteur` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_patient` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_naissance` datetime NOT NULL,
  `num_dossier` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `conclusion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_archived` tinyint(1) NOT NULL,
  `Criticite` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_2DB8C3173A201E5` (`createur_id`),
  KEY `IDX_2DB8C311E0D9F2` (`cahier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `fiche_patient`
--

INSERT INTO `fiche_patient` (`id`, `titre`, `cahier_id`, `date_creation`, `createur_id`, `date_archive`, `secteur`, `nom_patient`, `date_naissance`, `num_dossier`, `description`, `conclusion`, `is_archived`, `Criticite`) VALUES
(1, 'fiche patient numéro 1', 1, '2021-02-02 16:23:23', 1, NULL, 'oncologie', 'Labeau', '2020-10-06 16:23:23', 123456, 'Lorem ipsum', 'Lorem ipsum', 0, 'max frr'),
(2, 'fiche patient numéro 2', 2, '2020-11-15 16:23:23', 2, NULL, 'les jambes frr', 'Legrand', '2020-08-11 16:23:23', 456789, 'Lorem ipsum', 'Lorem ipsum', 0, 'max frr'),
(3, 'oui', 1, '2021-03-08 00:00:00', 1, NULL, 'oui', 'oui', '2021-03-07 00:00:00', 1, 'oui', 'oui', 0, 'max frr'),
(4, 'oui', 1, '2021-03-08 00:00:00', 1, NULL, 'oui', 'oui', '2021-03-07 00:00:00', 1, 'oui', 'oui', 0, 'max frr');

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `utilisateurs_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B63E2EC71E969C5` (`utilisateurs_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `roles`
--

INSERT INTO `roles` (`id`, `titre`, `utilisateurs_id`) VALUES
(1, 'Professeur', NULL),
(2, 'Médecin', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

DROP TABLE IF EXISTS `utilisateurs`;
CREATE TABLE IF NOT EXISTS `utilisateurs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `date_naissance` datetime NOT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `labo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_497B315ED60322AC` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`id`, `user_id`, `password`, `nom`, `prenom`, `role_id`, `date_naissance`, `telephone`, `labo`) VALUES
(1, '123456789', 'azerty', 'Leduc', 'Jean', 1, '2021-02-01 15:59:34', '0620202020', 'Rouen'),
(2, '234567891', 'qwerty', 'Leroux', 'Jacques', 2, '2021-01-12 15:59:34', '0310101010', 'Amiens');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `cahiers`
--
ALTER TABLE `cahiers`
  ADD CONSTRAINT `FK_91EFE3C573A201E5` FOREIGN KEY (`createur_id`) REFERENCES `utilisateurs` (`id`);

--
-- Contraintes pour la table `fiche_npatient`
--
ALTER TABLE `fiche_npatient`
  ADD CONSTRAINT `FK_41EB5FE41E0D9F2` FOREIGN KEY (`cahier_id`) REFERENCES `cahiers` (`id`),
  ADD CONSTRAINT `FK_41EB5FE473A201E5` FOREIGN KEY (`createur_id`) REFERENCES `utilisateurs` (`id`);

--
-- Contraintes pour la table `fiche_patient`
--
ALTER TABLE `fiche_patient`
  ADD CONSTRAINT `FK_2DB8C311E0D9F2` FOREIGN KEY (`cahier_id`) REFERENCES `cahiers` (`id`),
  ADD CONSTRAINT `FK_2DB8C3173A201E5` FOREIGN KEY (`createur_id`) REFERENCES `utilisateurs` (`id`);

--
-- Contraintes pour la table `roles`
--
ALTER TABLE `roles`
  ADD CONSTRAINT `FK_B63E2EC71E969C5` FOREIGN KEY (`utilisateurs_id`) REFERENCES `utilisateurs` (`id`);

--
-- Contraintes pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD CONSTRAINT `FK_497B315ED60322AC` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
