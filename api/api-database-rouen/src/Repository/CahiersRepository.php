<?php

namespace App\Repository;

use App\Entity\Cahiers;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Cahiers|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cahiers|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cahiers[]    findAll()
 * @method Cahiers[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CahiersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Cahiers::class);
    }

    // /**
    //  * @return Cahiers[] Returns an array of Cahiers objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Cahiers
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    /**
     * @return Cahiers[] Returns an array of Cahiers objects
    */
    public function findByDateArchived($value)
    {
        $condition = "".$value."%";
        return $this->createQueryBuilder('f')
            ->andWhere('f.DateArchive like :val')
            ->setParameter('val', $condition)
            ->orderBy('f.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findByRoles($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.Rattachement = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    
}
