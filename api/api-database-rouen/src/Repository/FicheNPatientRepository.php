<?php

namespace App\Repository;

use App\Entity\FicheNPatient;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FicheNPatient|null find($id, $lockMode = null, $lockVersion = null)
 * @method FicheNPatient|null findOneBy(array $criteria, array $orderBy = null)
 * @method FicheNPatient[]    findAll()
 * @method FicheNPatient[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FicheNPatientRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FicheNPatient::class);
    }

    // /**
    //  * @return FicheNPatient[] Returns an array of FicheNPatient objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FicheNPatient
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    /**
     * @return FicheNPatient[] Returns an array of FicheNPatient objects
    */
     public function findByDateArchived($value)
    {
        $condition = "".$value."%";
        return $this->createQueryBuilder('f')
            ->andWhere('f.DateArchive like :val')
            ->setParameter('val', $condition)
            ->orderBy('f.id', 'ASC')
            ->getQuery()
            ->getResult()
            
        ;
    }
}
