<?php

namespace App\Repository;

use App\Entity\FicheTypes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FicheTypes|null find($id, $lockMode = null, $lockVersion = null)
 * @method FicheTypes|null findOneBy(array $criteria, array $orderBy = null)
 * @method FicheTypes[]    findAll()
 * @method FicheTypes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FicheTypesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FicheTypes::class);
    }

    // /**
    //  * @return FicheTypes[] Returns an array of FicheTypes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FicheTypes
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
