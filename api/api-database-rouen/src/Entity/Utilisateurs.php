<?php

namespace App\Entity;

use App\Entity\Roles;
use App\Repository\UtilisateursRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=UtilisateursRepository::class)
 * @ORM\Table(name="utilisateurs")
 */
class Utilisateurs
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"list_utilisateurs_principal","list_utilisateurs_secondaire"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"list_utilisateurs_principal"})
     */
    private $Password;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"list_utilisateurs_principal","list_utilisateurs_secondaire"})
     */
    private $Nom;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"list_utilisateurs_principal","list_utilisateurs_secondaire"})
     */
    private $Prenom;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"list_utilisateurs_principal"})
     */
    private $Labo;

    /**
     * @Groups({"list_utilisateurs_principal"})
     * @ORM\OneToMany(targetEntity=Cahiers::class, mappedBy="Createur")
     * @Groups({"list_utilisateurs_principal"})
     */
    private $cahiers;

    /**
     * @ORM\OneToMany(targetEntity=FichePatient::class, mappedBy="Createur")
     * @Groups({"list_utilisateurs_principal"})
     */
    private $fichesPatient;

    /**
     * @ORM\OneToMany(targetEntity=FicheNPatient::class, mappedBy="Createur")
     * @Groups({"list_utilisateurs_principal"})
     */
    private $fichesNPatient;

    /**
     * @Groups({"list_utilisateurs_principal","list_utilisateurs_secondaire"})
     * @ORM\Column(type="string", length=255)
     */
    private $User_ID;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"list_utilisateurs_principal"})
     */
    private $last_connexion;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"list_utilisateurs_principal"})
     */
    private $is_active;

    /**
     * @var \Doctrine\Common\Collections\Collection|Roles[]
     * @ORM\ManyToMany(targetEntity="Roles", inversedBy="Utilisateurs", cascade={"persist"})
     * @ORM\JoinTable(
     * name="utilisateurs_roles",
     * joinColumns={
     *  @ORM\JoinColumn(name="utilisateurs_id", referencedColumnName="id")
     * },
     * inverseJoinColumns={
     *  @ORM\JoinColumn(name="roles_id", referencedColumnName="id")
     * }
     * )
     * @Groups({"list_utilisateurs_principal"})
     */
    private $Roles;

    public function __construct()
    {
        $this->cahiers = new ArrayCollection();
        $this->fichesPatient = new ArrayCollection();
        $this->fichesNPatient = new ArrayCollection();
        $this->Role = new ArrayCollection();
        $this->Roles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPassword(): ?string
    {
        return $this->Password;
    }

    public function setPassword(string $Password): self
    {
        $this->Password = $Password;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->Nom;
    }

    public function setNom(string $Nom): self
    {
        $this->Nom = $Nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->Prenom;
    }

    public function setPrenom(string $Prenom): self
    {
        $this->Prenom = $Prenom;

        return $this;
    }

    public function getLabo(): ?string
    {
        return $this->Labo;
    }

    public function setLabo(string $Labo): self
    {
        $this->Labo = $Labo;

        return $this;
    }

    /**
     * @return Collection|Cahiers[]
     */
    public function getCahiers(): Collection
    {
        return $this->cahiers;
    }

    public function addCahier(Cahiers $cahier): self
    {
        if (!$this->cahiers->contains($cahier)) {
            $this->cahiers[] = $cahier;
            $cahier->setCreateur($this);
        }

        return $this;
    }

    public function removeCahier(Cahiers $cahier): self
    {
        if ($this->cahiers->removeElement($cahier)) {
            // set the owning side to null (unless already changed)
            if ($cahier->getCreateur() === $this) {
                $cahier->setCreateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FichePatient[]
     */
    public function getFichesPatient(): Collection
    {
        return $this->fichesPatient;
    }

    public function addFichesPatient(FichePatient $fichesPatient): self
    {
        if (!$this->fichesPatient->contains($fichesPatient)) {
            $this->fichesPatient[] = $fichesPatient;
            $fichesPatient->setCreateur($this);
        }

        return $this;
    }

    public function removeFichesPatient(FichePatient $fichesPatient): self
    {
        if ($this->fichesPatient->removeElement($fichesPatient)) {
            // set the owning side to null (unless already changed)
            if ($fichesPatient->getCreateur() === $this) {
                $fichesPatient->setCreateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FicheNPatient[]
     */
    public function getFichesNPatient(): Collection
    {
        return $this->fichesNPatient;
    }

    public function addFichesNPatient(FicheNPatient $fichesNPatient): self
    {
        if (!$this->fichesNPatient->contains($fichesNPatient)) {
            $this->fichesNPatient[] = $fichesNPatient;
            $fichesNPatient->setCreateur($this);
        }

        return $this;
    }

    public function removeFichesNPatient(FicheNPatient $fichesNPatient): self
    {
        if ($this->fichesNPatient->removeElement($fichesNPatient)) {
            // set the owning side to null (unless already changed)
            if ($fichesNPatient->getCreateur() === $this) {
                $fichesNPatient->setCreateur(null);
            }
        }

        return $this;
    }

    public function getUserID(): ?string
    {
        return $this->User_ID;
    }

    public function setUserID(string $User_ID): self
    {
        $this->User_ID = $User_ID;

        return $this;
    }

    public function __toString(): string
    {
        return $this->getNom()." ".$this->getPrenom();
    }

    public function getLastConnexion(): ?\DateTimeInterface
    {
        return $this->last_connexion;
    }

    public function setLastConnexion($last_connexion): self
    {
        if (gettype($last_connexion) == "string") {
            $this->last_connexion = new \DateTime(date('Y-m-d', strtotime($last_connexion)));
        } else if (gettype($last_connexion) != "string") {
            $this->last_connexion = $last_connexion;
        }

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(?bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    /**
     * @return Collection|Roles[]
     */
    public function getRoles(): Collection
    {
        return $this->Roles;
    }

    /**
     * @param Roles $role
     */
    public function addRole(Roles $role)
    {
        /*if (!$this->Roles->contains($role)) {
            $this->Roles[] = $role;
            $role->addUtilisateur($this);
        }

        return $this;*/
        if ($this->Roles->contains($role)) {
            return;
        }

        $this->Roles->add($role);
        $role->addUtilisateur($this);

    }

    public function removeRole(Roles $role): self
    {
        if ($this->Roles->removeElement($role)) {
            $role->removeUtilisateur($this);
        }

        return $this;
    }
}
