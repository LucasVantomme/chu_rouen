<?php

namespace App\Entity;

use App\Repository\FichePatientRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use \DateTime;


/**
 * @ORM\Entity(repositoryClass=FichePatientRepository::class)
 */
class FichePatient
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"list_fiches_principal","list_fiches_secondaire","list_fiches_special"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"list_fiches_principal","list_fiches_secondaire","list_fiches_special"})
     */
    private $Titre;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"list_fiches_principal","list_fiches_secondaire","list_fiches_special"})
     */
    private $DateCreation;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"list_fiches_principal","list_fiches_secondaire","list_fiches_special"})
     */
    private $Secteur;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"list_fiches_principal","list_fiches_secondaire","list_fiches_special"})
     */
    private $NomPatient;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"list_fiches_principal","list_fiches_secondaire","list_fiches_special"})
     */
    private $DateNaissance;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"list_fiches_principal","list_fiches_secondaire","list_fiches_special"})
     */
    private $NumDossier;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"list_fiches_principal","list_fiches_secondaire","list_fiches_special"})
     */
    private $isArchived;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateurs::class, inversedBy="fichesPatient",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     *  @Groups({"list_fiches_principal","list_fiches_secondaire"})
     */
    private $Createur;

    /**
     * @ORM\ManyToOne(targetEntity=Cahiers::class, inversedBy="fichesPatient",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"list_fiches_principal"})
     */
    private $Cahier;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"list_fiches_principal","list_fiches_secondaire","list_fiches_special"})
     */
    private $DateArchive;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"list_fiches_principal","list_fiches_secondaire","list_fiches_special"})
     */
    private $Description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"list_fiches_principal","list_fiches_secondaire","list_fiches_special"})
     */
    private $criticite;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"list_fiches_principal","list_fiches_secondaire","list_fiches_special"})
     */
    private $Conclusion;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $url_file;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->Titre;
    }

    public function setTitre(string $Titre): self
    {
        $this->Titre = $Titre;

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->DateCreation;
    }

    public function setDateCreation( $DateCreation): self
    {
        if (gettype($DateCreation) == "string") {
            $this->DateCreation = new \DateTime(date('Y-m-d', strtotime($DateCreation)));
        } else if (gettype($DateCreation) != "string") {
            $this->DateCreation = $DateCreation;
        }

        return $this;
    }

    public function getSecteur(): ?string
    {
        return $this->Secteur;
    }

    public function setSecteur(string $Secteur): self
    {
        $this->Secteur = $Secteur;

        return $this;
    }

    public function getNomPatient(): ?string
    {
        return $this->NomPatient;
    }

    public function setNomPatient(string $NomPatient): self
    {
        $this->NomPatient = $NomPatient;

        return $this;
    }

    public function getDateNaissance(): ?\DateTimeInterface
    {
        return $this->DateNaissance;
    }

    public function setDateNaissance($DateNaissance): self
    {
        if (gettype($DateNaissance) == "string") {
            $this->DateNaissance = new \DateTime(date('Y-m-d', strtotime($DateNaissance)));
        } else if (gettype($DateNaissance) != "string") {
            $this->DateNaissance = $DateNaissance;
        }

        return $this;
    }

    public function getNumDossier(): ?int
    {
        return $this->NumDossier;
    }

    public function setNumDossier(int $NumDossier): self
    {
        $this->NumDossier = $NumDossier;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getConclusion(): ?string
    {
        return $this->Conclusion;
    }

    public function setConclusion(string $Conclusion): self
    {
        $this->Conclusion = $Conclusion;

        return $this;
    }

    public function getIsArchived(): ?bool
    {
        return $this->isArchived;
    }

    public function setIsArchived(bool $isArchived): self
    {
        $this->isArchived = $isArchived;

        return $this;
    }

    public function getCreateur(): ?Utilisateurs
    {
        return $this->Createur;
    }

    public function setCreateur(?Utilisateurs $Createur): self
    {
        $this->Createur = $Createur;

        return $this;
    }

    public function getCahier(): ?Cahiers
    {
        return $this->Cahier;
    }

    public function setCahier(?Cahiers $Cahier): self
    {
        $this->Cahier = $Cahier;

        return $this;
    }

    public function getDateArchive(): ?\DateTimeInterface
    {
        return $this->DateArchive;
    }

    public function setDateArchive($DateArchive): self
    {
        if ($DateArchive == null) {
            $this->DateArchive = null;
        } else {
            $this->DateArchive = new \DateTime(date('Y-m-d', strtotime($DateArchive)));
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->getTitre();
    }

    public function getCriticite(): ?string
    {
        return $this->criticite;
    }

    public function setCriticite(string $criticite): self
    {
        $this->criticite = $criticite;

        return $this;
    }

    public function getUrlFile(): ?string
    {
        return $this->url_file;
    }

    public function setUrlFile(?string $url_file): self
    {
        $this->url_file = $url_file;

        return $this;
    }
}
