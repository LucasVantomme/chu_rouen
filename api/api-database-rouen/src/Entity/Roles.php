<?php

namespace App\Entity;

use App\Repository\RolesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=RolesRepository::class)
 * @ORM\Table(name="roles")
 */
class Roles
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"list_roles","list_roles_second"})
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"list_roles","list_roles_second"})
     */
    private $Titre;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"list_roles","list_roles_second"})
     */
    private $is_active = true;

    /**
     * @var \Doctrine\Common\Collections\Collection|User[]
     * @ORM\ManyToMany(targetEntity="Utilisateurs", mappedBy="Roles")
     * @Groups({"list_roles"})
     */
    private $Utilisateurs;

    public function __construct()
    {
        $this->utilisateurs = new ArrayCollection();
        $this->Utilisateurs = new ArrayCollection();
        $this->Utilisateur = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->Titre;
    }

    public function setTitre(string $Titre): self
    {
        $this->Titre = $Titre;

        return $this;
    }

    public function __toString(): string
    {
        return $this->getId()." ".$this->getTitre();
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(?bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }

    /**
     * @return Collection|Utilisateurs[]
     */
    public function getUtilisateurs(): Collection
    {
        return $this->Utilisateurs;
    }

    /**
     * @param Utilisateurs $utilisateur
     */
    public function addUtilisateur(Utilisateurs $utilisateur)
    {
        if (!$this->Utilisateurs->contains($utilisateur)) {
            return;    
        }
        $this->Utilisateurs[] = $utilisateur;
        return;
    }

    /**
     * @param Utilisateurs $utilisateur
     */
    public function removeUtilisateur(Utilisateurs $utilisateur): self
    {
        $this->Utilisateurs->removeElement($utilisateur);

        return $this;
    }
}
