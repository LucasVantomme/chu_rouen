<?php

namespace App\Entity;

use App\Repository\CahiersRepository;
use App\Entity\FichePatient;
use App\Entity\Utilisateurs;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=CahiersRepository::class)
 */
class Cahiers
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"list_cahiers_principal","list_cahiers_secondaire","list_cahiers_special"})
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"list_cahiers_principal","list_cahiers_secondaire","list_cahiers_special"})
     */
    private $DateCreation;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"list_cahiers_principal","list_cahiers_secondaire","list_cahiers_special"})
     */
    private $Type;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"list_cahiers_principal","list_cahiers_secondaire","list_cahiers_special"})
     */
    private $Rattachement;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"list_cahiers_principal","list_cahiers_secondaire","list_cahiers_special"})
     */
    private $isArchived = 0;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateurs::class, inversedBy="cahiers", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"list_cahiers_principal","list_cahiers_secondaire"})
     */
    private $Createur;

    /**
     * @ORM\OneToMany(targetEntity=FichePatient::class, mappedBy="Cahier", cascade={"persist"})
     * @Groups({"list_cahiers_principal"})
     */
    private $fichesPatient;

    /**
     * @ORM\OneToMany(targetEntity=FicheNPatient::class, mappedBy="Cahier", cascade={"persist"})
     * @Groups({"list_cahiers_principal"})
     */
    private $fichesNPatient;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"list_cahiers_principal","list_cahiers_secondaire","list_cahiers_special"})
     */
    private $DateArchive;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"list_cahiers_principal","list_cahiers_secondaire","list_cahiers_special"})
     */
    private $Nom;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"list_cahiers_principal","list_cahiers_secondaire","list_cahiers_special"})
     */
    private $is_active = true;

    public function __construct()
    {
        $this->fichesPatient = new ArrayCollection();
        $this->fichesNPatient = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->DateCreation;
    }

    public function setDateCreation(\DateTimeInterface $DateCreation): self
    {
        $this->DateCreation = $DateCreation;

        return $this;
    }

    public function getCreateur(): ?Utilisateurs
    {
        return $this->Createur;
    }

    public function setCreateur(?Utilisateurs $Createur): self
    {
        $this->Createur = $Createur;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->Type;
    }

    public function setType(string $Type): self
    {
        $this->Type = $Type;

        return $this;
    }

    public function getRattachement(): ?string
    {
        return $this->Rattachement;
    }

    public function setRattachement(string $Rattachement): self
    {
        $this->Rattachement = $Rattachement;

        return $this;
    }

    public function getIsArchived(): ?bool
    {
        return $this->isArchived;
    }

    public function setIsArchived(bool $isArchived): self
    {
        $this->isArchived = $isArchived;

        return $this;
    }

    /**
     * Get FichePatient
     * @return Doctrine\Common\Collections\Collection
     */
    public function getFichesPatient()
    {
        return $this->fichesPatient;
    }

    public function addFichesPatient(FichePatient $fichesPatient): self
    {
        //if (!$this->fichesPatient->contains($fichesPatient)) {
            $this->fichesPatient[] = $fichesPatient;
            $fichesPatient->setCahier($this);
        //}

        return $this;
    }

    public function removeFichesPatient(FichePatient $fichesPatient): self
    {
        if ($this->fichesPatient->removeElement($fichesPatient)) {
            // set the owning side to null (unless already changed)
            if ($fichesPatient->getCahier() === $this) {
                $fichesPatient->setCahier(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FicheNPatient[]
     */
    public function getFichesNPatient(): Collection
    {
        return $this->fichesNPatient;
    }

    public function addFichesNPatient(FicheNPatient $fichesNPatient): self
    {
        if (!$this->fichesNPatient->contains($fichesNPatient)) {
            $this->fichesNPatient[] = $fichesNPatient;
            $fichesNPatient->setCahier($this);
        }

        return $this;
    }

    public function removeFichesNPatient(FicheNPatient $fichesNPatient): self
    {
        if ($this->fichesNPatient->removeElement($fichesNPatient)) {
            // set the owning side to null (unless already changed)
            if ($fichesNPatient->getCahier() === $this) {
                $fichesNPatient->setCahier(null);
            }
        }

        return $this;
    }

    public function getDateArchive(): ?\DateTimeInterface
    {
        return $this->DateArchive;
    }

    public function setDateArchive(?\DateTimeInterface $DateArchive): self
    {
        
        $this->DateArchive = $DateArchive;
        return $this;
    }

    public function getNom(): ?string
    {
        return $this->Nom;
    }

    public function setNom(string $Nom): self
    {
        $this->Nom = $Nom;

        return $this;
    }

    public function __toString(): string
    {
        return " ".$this->getNom();
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(?bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }
}
