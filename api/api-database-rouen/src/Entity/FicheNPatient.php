<?php

namespace App\Entity;

use App\Repository\FicheNPatientRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use \DateTime;

/**
 * @ORM\Entity(repositoryClass=FicheNPatientRepository::class)
 */
class FicheNPatient
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"list_Nfiches_principal","list_Nfiches_secondaire","list_Nfiches_special"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"list_Nfiches_principal","list_Nfiches_secondaire","list_Nfiches_special"})
     */
    private $Titre;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"list_Nfiches_principal","list_Nfiches_secondaire","list_Nfiches_special"})
     */
    private $DateCreation;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"list_Nfiches_principal","list_Nfiches_secondaire","list_Nfiches_special"})
     */
    private $Secteur;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"list_Nfiches_principal","list_Nfiches_secondaire","list_Nfiches_special"})
     */
    private $Criticite;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"list_Nfiches_principal","list_Nfiches_secondaire","list_Nfiches_special"})
     */
    private $Type;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"list_Nfiches_principal","list_Nfiches_secondaire","list_Nfiches_special"})
     */
    private $isArchived;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateurs::class, inversedBy="fichesNPatient")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"list_Nfiches_principal","list_Nfiches_secondaire"})
     */
    private $Createur;

    /**
     * @ORM\ManyToOne(targetEntity=Cahiers::class, inversedBy="fichesNPatient")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"list_Nfiches_principal"})
     */
    private $Cahier;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"list_Nfiches_principal","list_Nfiches_secondaire","list_Nfiches_special"})
     */
    private $DateArchive;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"list_Nfiches_principal","list_Nfiches_secondaire","list_Nfiches_special"})
     */
    private $Conclusion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"list_Nfiches_principal","list_Nfiches_secondaire","list_Nfiches_special"})
     */
    private $Description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"list_Nfiches_principal","list_Nfiches_secondaire","list_Nfiches_special"})
     */
    private $TexteLibre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $url_file;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->Titre;
    }

    public function setTitre(string $Titre): self
    {
        $this->Titre = $Titre;

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->DateCreation;
    }

    public function setDateCreation($DateCreation): self
    {
        if (gettype($DateCreation) == "string") {
            $this->DateCreation = new \DateTime(date('Y-m-d', strtotime($DateCreation)));
        } else if (gettype($DateCreation) != "string") {
            $this->DateCreation = $DateCreation;
        }

        return $this;
    }

    public function getSecteur(): ?string
    {
        return $this->Secteur;
    }

    public function setSecteur(string $Secteur): self
    {
        $this->Secteur = $Secteur;

        return $this;
    }

    public function getCriticite(): ?string
    {
        return $this->Criticite;
    }

    public function setCriticite(string $Criticite): self
    {
        $this->Criticite = $Criticite;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->Type;
    }

    public function setType(string $Type): self
    {
        $this->Type = $Type;

        return $this;
    }

    public function getTexteLibre(): ?string
    {
        return $this->TexteLibre;
    }

    public function setTexteLibre(string $TexteLibre): self
    {
        $this->TexteLibre = $TexteLibre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getConclusion(): ?string
    {
        return $this->Conclusion;
    }

    public function setConclusion(string $Conclusion): self
    {
        $this->Conclusion = $Conclusion;

        return $this;
    }

    public function getIsArchived(): ?bool
    {
        return $this->isArchived;
    }

    public function setIsArchived(bool $isArchived): self
    {
        $this->isArchived = $isArchived;

        return $this;
    }

    public function getCreateur(): ?Utilisateurs
    {
        return $this->Createur;
    }

    public function setCreateur(?Utilisateurs $Createur): self
    {
        $this->Createur = $Createur;

        return $this;
    }

    public function getCahier(): ?Cahiers
    {
        return $this->Cahier;
    }

    public function setCahier(?Cahiers $Cahier): self
    {
        $this->Cahier = $Cahier;

        return $this;
    }

    public function getDateArchive(): ?\DateTimeInterface
    {
        return $this->DateArchive;
    }

    public function setDateArchive($DateArchive): self
    {
        if ($DateArchive == null) {
            $this->DateArchive = null;
        } else {
            $this->DateArchive = new \DateTime(date('Y-m-d', strtotime($DateArchive)));
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->getTitre();
    }

    public function getUrlFile(): ?string
    {
        return $this->url_file;
    }

    public function setUrlFile(?string $url_file): self
    {
        $this->url_file = $url_file;

        return $this;
    }
}
