<?php

namespace App\Controller;

use App\Entity\Secteur;
use App\Repository\SecteurRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class SecteurController extends AbstractController
{
    public function listSecteurs(SecteurRepository $SecteursRepository, SerializerInterface $serializer): JsonResponse
    {
        $Secteurs = $SecteursRepository->findAll();
        $json = $serializer->serialize(
            $Secteurs,
            'json',
            ['groups' => 'list_Secteurs']
        );
        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

    public function secteurGetById(SerializerInterface $serializer, EntityManagerInterface $entityManager, Secteur $secteur): JsonResponse
    {
        $SecteurToGet = $entityManager->getRepository(Secteur::class)->find($secteur->getId());
        $json = $serializer->serialize(
            $SecteurToGet,
            'json',
            ['groups' => 'list_Secteurs']
        );
        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

    public function addSecteur(Request $request, SerializerInterface $serializer, EntityManagerInterface $entityManager, ValidatorInterface $validator){
        $Secteur = $serializer->deserialize($request->getContent(), Secteur::class, 'json');
        $errors = $validator->validate($Secteur);
        if(count($errors)) {
            $errors = $serializer->serialize($errors, 'json');
            return new Response($errors, 500, [
                'Content-Type' => 'application/json'
            ]);
        }
        $entityManager->persist($Secteur);
        $entityManager->flush();
        $data = [
            'status' => 201,
            'message' => 'Le Secteur a bien ete ajoute'
        ];
        return new JsonResponse($data, 201);
    }

    public function updateSecteur(Request $request, SerializerInterface $serializer, Secteur $Secteur, ValidatorInterface $validator, EntityManagerInterface $entityManager)
    {
        $SecteurUpdate = $entityManager->getRepository(Secteur::class)->find($Secteur->getId());
        $data = json_decode($request->getContent());
        foreach ($data as $key => $value){
            if($key && !empty($value)) {
                $name = ucfirst($key);
                $setter = 'set'.$name;
                $SecteurUpdate->$setter($value);
            }
        }
        $errors = $validator->validate($SecteurUpdate);
        if(count($errors)) {
            $errors = $serializer->serialize($errors, 'json');
            return new Response($errors, 500, [
                'Content-Type' => 'application/json'
            ]);
        }
        $entityManager->flush();
        $data = [
            'status' => 200,
            'message' => 'Le Secteur a bien ete mis a jour'
        ];
        return new JsonResponse($data);
    }

    public function deleteSecteur(Secteur $Secteur, EntityManagerInterface $entityManager)
    {
        $entityManager->remove($Secteur);
        $entityManager->flush();
        return new Response(null, 204);
    }
}
