<?php

namespace App\Controller;

use App\Entity\Roles;
use App\Entity\Utilisateurs;
use App\Repository\UtilisateursRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
//use App\Controller\RolesController;

class UtilisateursController extends AbstractController
{
    public function listUtilisateurs(UtilisateursRepository $utilisateursRepository, SerializerInterface $serializer): JsonResponse
    {
        $utilisateurs = $utilisateursRepository->findAll();
        $json = $serializer->serialize(
            $utilisateurs,
            'json',
            ['groups' => ['list_utilisateurs_principal',
              'list_fiches_special','list_Nfiches_special','list_cahiers_special','list_roles_second']]
        );
        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

    public function utilisateurGetById(SerializerInterface $serializer, EntityManagerInterface $entityManager, Utilisateurs $utilisateur): JsonResponse
    {
        $utilisateurToGet = $entityManager->getRepository(Utilisateurs::class)->find($utilisateur->getId());
        $json = $serializer->serialize(
            $utilisateurToGet,
            'json',
            ['groups' => ['list_utilisateurs_principal',
              'list_fiches_special','list_Nfiches_special','list_cahiers_special','list_roles_second']]
        );
        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

    public function addUtilisateur(Request $request, SerializerInterface $serializer, EntityManagerInterface $entityManager, ValidatorInterface $validator){
        $content =$request->getContent();
        $json = json_decode($content);
        $utilisateur = $serializer->deserialize($request->getContent(), Utilisateurs::class, 'json');
        
        foreach ($json->Roles as $key => $value) {
            $role = $entityManager->getRepository(Roles::class)->find($value->id);
            $utilisateur->addRole($role);
        }
        $errors = $validator->validate($utilisateur);
        if(count($errors)) {
            $errors = $serializer->serialize($errors, 'json');
            return new Response($errors, 500, [
                'Content-Type' => 'application/json'
            ]);
        }
        $entityManager->persist($utilisateur);
        $entityManager->flush();

        $rolesTable = $entityManager->getRepository(Roles::class)->findAll();
        $i = 0;

        foreach ($json->Roles as $key => $value) {
            
            $roleToDelete = $rolesTable[count($rolesTable)-1-$i];
            $entityManager->remove($roleToDelete);
            $i++;
        }
        $entityManager->flush();

        $data = [
            'status' => 201,
            'message' => $utilisateur
        ];
        return new JsonResponse($data, 201);
    }

    public function updateUtilisateur(Request $request, SerializerInterface $serializer, Utilisateurs $utilisateur, ValidatorInterface $validator, EntityManagerInterface $entityManager)
    {
        
        $utilisateurUpdate = $entityManager->getRepository(Utilisateurs::class)->find($utilisateur->getId());
        $data = json_decode($request->getContent());
        foreach ($data as $key => $value){
            if($key && !empty($value)) {
                $name = ucfirst($key);
                $setter = 'set'.$name;
                $utilisateurUpdate->$setter($value);
            }
        }
        $errors = $validator->validate($utilisateurUpdate);
        if(count($errors)) {
            $errors = $serializer->serialize($errors, 'json');
            return new Response($errors, 500, [
                'Content-Type' => 'application/json'
            ]);
        }
        $entityManager->flush();
        $data = [
            'status' => 200,
            'message' => 'Utilisateur mis a jour'
        ];
        return new JsonResponse($data);
    }

    public function deleteUtilisateur(Utilisateurs $utilisateur, EntityManagerInterface $entityManager)
    {
        $entityManager->remove($utilisateur);
        $entityManager->flush();
        return new Response(null, 204);
    }

    public function utilisateurConnection(SerializerInterface $serializer, EntityManagerInterface $entityManager, $id_user, $password): JsonResponse
    {
        $utilisateurToGet = $entityManager->getRepository(Utilisateurs::class)->findByPassword($id_user,$password);
        $json = $serializer->serialize(
            $utilisateurToGet,
            'json',
            ['groups' => ['list_utilisateurs_principal',
              'list_fiches_special','list_Nfiches_special','list_cahiers_special','list_roles_second']]
        );
        $dateConnexion = new DateTime();
        $dateConnexion->format('Y-m-d');
        
        try {
            if ($utilisateurToGet[0]->getIsActive() == true) {
                $utilisateurToGet[0]->setLastConnexion($dateConnexion);
                $entityManager->persist($utilisateurToGet[0]);
                $entityManager->flush();
                return new JsonResponse($json, Response::HTTP_OK, [], true);
            }else{
                $data = [
                    'status' => 401,
                    'message' => 'Utilisateur desactivé'
                ];
                return new JsonResponse($data, 401);
            }
        } catch (\Throwable $th) {
            $data = [
                'status' => 404,
                'message' => 'Utilisateur non trouvé'
            ];
            return new JsonResponse($data, 404);
        }
        
    }
}
