<?php

namespace App\Controller;

use App\Entity\Roles;
use App\Repository\RolesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class RolesController extends AbstractController
{
    public function listRoles(RolesRepository $rolesRepository, SerializerInterface $serializer): JsonResponse
    {
        $roles = $rolesRepository->findByActive();
        $json = $serializer->serialize(
            $roles,
            'json',
            ['groups' => 'list_roles','list_roles_principal']
        );
        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

    public function roleGetById(SerializerInterface $serializer, EntityManagerInterface $entityManager, Roles $role): JsonResponse
    {
        $roleToGet = $entityManager->getRepository(Roles::class)->find($role->getId());
        $json = $serializer->serialize(
            $roleToGet,
            'json',
            ['groups' => 'list_roles','list_roles_principal']
        );
        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

    public function addRole(Request $request, SerializerInterface $serializer, EntityManagerInterface $entityManager, ValidatorInterface $validator){
        $role = $serializer->deserialize($request->getContent(), Roles::class, 'json');
        $errors = $validator->validate($role);
        if(count($errors)) {
            $errors = $serializer->serialize($errors, 'json');
            return new Response($errors, 500, [
                'Content-Type' => 'application/json'
            ]);
        }
        $entityManager->persist($role);
        $entityManager->flush();
        $data = [
            'status' => 201,
            'message' => 'Le role a bien ete ajoute'
        ];
        return new JsonResponse($data, 201);
    }

    public function updateRole(Request $request, SerializerInterface $serializer, Roles $role, ValidatorInterface $validator, EntityManagerInterface $entityManager)
    {
        $roleUpdate = $entityManager->getRepository(Roles::class)->find($role->getId());
        $data = json_decode($request->getContent());
        foreach ($data as $key => $value){
            if($key && !empty($value)) {
                $name = ucfirst($key);
                $setter = 'set'.$name;
                $roleUpdate->$setter($value);
            }
        }
        $errors = $validator->validate($roleUpdate);
        if(count($errors)) {
            $errors = $serializer->serialize($errors, 'json');
            return new Response($errors, 500, [
                'Content-Type' => 'application/json'
            ]);
        }
        $entityManager->flush();
        $data = [
            'status' => 200,
            'message' => 'Le role a bien ete mis a jour'
        ];
        return new JsonResponse($data);
    }

    public function deleteRole(Roles $role, EntityManagerInterface $entityManager)
    {
        $entityManager->remove($role);
        $entityManager->flush();
        return new Response(null, 204);
    }
}
