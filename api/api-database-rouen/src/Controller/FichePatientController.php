<?php

namespace App\Controller;

use App\Entity\FichePatient;
use App\Entity\Utilisateurs;
use App\Entity\Cahiers;
use App\Repository\FichePatientRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use DateTime;

class FichePatientController extends AbstractController 
{
    public function listFiches(FichePatientRepository $fichesRepository, SerializerInterface $serializer): JsonResponse
    {
        $fiches = $fichesRepository->findAll();
        
        $json = $serializer->serialize(
            $fiches,
            'json',
            ['groups' => ['list_fiches_principal',
              'list_cahiers_secondaire','list_utilisateurs_secondaire']]
        );
        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

    public function ficheGetById(SerializerInterface $serializer, EntityManagerInterface $entityManager, FichePatient $fiche): JsonResponse
    {
        $ficheToGet = $entityManager->getRepository(FichePatient::class)->find($fiche->getId());
        $json = $serializer->serialize(
            $ficheToGet,
            'json',
            ['groups' => ['list_fiches_principal',
              'list_cahiers_secondaire','list_utilisateurs_secondaire']]
        );
        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

    public function ficheGetByDateArchive(SerializerInterface $serializer, EntityManagerInterface $entityManager, $date): JsonResponse
    {
        $ficheToGet = $entityManager->getRepository(FichePatient::class)->findByDateArchived($date);
        $json = $serializer->serialize(
            $ficheToGet,
            'json',
            ['groups' => ['list_fiches_principal',
              'list_cahiers_secondaire','list_utilisateurs_secondaire']]
        );
        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

    public function addFiche(Request $request, SerializerInterface $serializer, EntityManagerInterface $entityManager, ValidatorInterface $validator){
        //récuperation de l'id du createur
        $content =$request->getContent();
        $json = json_decode($content);
        $createur =  $entityManager->getRepository(Utilisateurs::class)->findByUserID($json->Createur->User_ID);
        
        //récuperation de l'id du cahier
        $cahier =  $entityManager->getRepository(Cahiers::class)->find($json->Cahier->id);
        $fiche = $serializer->deserialize($request->getContent(), FichePatient::class, 'json');
        $fiche->setCreateur($createur);
        $fiche->setCahier($cahier);

        $errors = $validator->validate($fiche);
        if(count($errors)) {
            $errors = $serializer->serialize($errors, 'json');
            return new Response($errors, 500, [
                'Content-Type' => 'application/json'
            ]);
        }
        $dateCreation = new DateTime();
        $dateCreation->format('Y-m-d');
        $fiche->setDateCreation($dateCreation);
        $entityManager->persist($fiche);
        $entityManager->flush();
        $data = [
            'status' => 201,
            'message' => 'Fiche bien ajoute'
        ];
        return new JsonResponse($data, 201);
    }

    public function updateFiche(Request $request, SerializerInterface $serializer, FichePatient $fiche, ValidatorInterface $validator, EntityManagerInterface $entityManager)
    {
        $ficheUpdate = $entityManager->getRepository(FichePatient::class)->find($fiche->getId());
        $data = json_decode($request->getContent());
        foreach ($data as $key => $value){
            $name = ucfirst($key);
            $setter = 'set'.$name;
            $ficheUpdate->$setter($value);
        }
        $errors = $validator->validate($ficheUpdate);
        if(count($errors)) {
            $errors = $serializer->serialize($errors, 'json');
            return new Response($errors, 500, [
                'Content-Type' => 'application/json'
            ]);
        }
        $entityManager->flush();
        $data = [
            'status' => 200,
            'message' => 'Fiche mis a jour'
        ];
        return new JsonResponse($data);
    }

    public function deleteFiche(FichePatient $fiche, EntityManagerInterface $entityManager)
    {
        $entityManager->remove($fiche);
        $entityManager->flush();
        return new Response(null, 204);
    }

    public function uploadFileFiche(Request $request, SerializerInterface $serializer, ValidatorInterface $validator, EntityManagerInterface $entityManager)
    {
        $url = "C:/test_doc";
        
        //$data = json_decode($request->getContent());
        //get the file
        //$file=$_FILES['file']['name'];

        return new JsonResponse($request->getContent());

        /*

        //get file extension
        $ext = pathinfo($file, PATHINFO_EXTENSION);

        //set random unique name why because file name duplicate will replace
        //the existing files
        $modified_fname=uniqid(rand(10,200)).'-'.rand(1000,1000000).'-'.$file;

        //set target file path
        $target_path = $url . basename($modified_fname).".".$ext;
        move_uploaded_file($data['file']['tmp_name'], $target_path);

        $ficheUpdate = $entityManager->getRepository(FichePatient::class)->find($fiche->getId());

        $tab = ["url_file" => $target_path];
        foreach ($tab as $key => $value){
            if($key && !empty($value)) {
                $name = ucfirst($key);
                $setter = 'set'.$name;
                $ficheUpdate->$setter($value);
            }
        }

        $errors = $validator->validate($ficheUpdate);
        if(count($errors)) {
            $errors = $serializer->serialize($errors, 'json');
            return new Response($errors, 500, [
                'Content-Type' => 'application/json'
            ]);
        }
        $entityManager->flush();
        $data = [
            'status' => 200,
            'message' => 'Utilisateur mis a jour'
        ];
        return new JsonResponse($data);*/
    }
}
