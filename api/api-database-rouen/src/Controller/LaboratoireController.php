<?php

namespace App\Controller;

use App\Entity\Laboratoire;
use App\Repository\LaboratoireRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class LaboratoireController extends AbstractController
{
    public function listLabos(LaboratoireRepository $labosRepository, SerializerInterface $serializer): JsonResponse
    {
        $labos = $labosRepository->findAll();
        $json = $serializer->serialize(
            $labos,
            'json',
            ['groups' => 'list_labos']
        );
        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

    public function laboGetById(SerializerInterface $serializer, EntityManagerInterface $entityManager, Laboratoire $labo): JsonResponse
    {
        $laboToGet = $entityManager->getRepository(Laboratoire::class)->find($labo->getId());
        $json = $serializer->serialize(
            $laboToGet,
            'json',
            ['groups' => 'list_labos']
        );
        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

    public function addLabo(Request $request, SerializerInterface $serializer, EntityManagerInterface $entityManager, ValidatorInterface $validator){
        $labo = $serializer->deserialize($request->getContent(), Laboratoire::class, 'json');
        $errors = $validator->validate($labo);
        if(count($errors)) {
            $errors = $serializer->serialize($errors, 'json');
            return new Response($errors, 500, [
                'Content-Type' => 'application/json'
            ]);
        }
        $entityManager->persist($labo);
        $entityManager->flush();
        $data = [
            'status' => 201,
            'message' => 'Le labo a bien ete ajoute'
        ];
        return new JsonResponse($data, 201);
    }

    public function updateLabo(Request $request, SerializerInterface $serializer, Laboratoire $labo, ValidatorInterface $validator, EntityManagerInterface $entityManager)
    {
        $laboUpdate = $entityManager->getRepository(Laboratoire::class)->find($labo->getId());
        $data = json_decode($request->getContent());
        foreach ($data as $key => $value){
            if($key && !empty($value)) {
                $name = ucfirst($key);
                $setter = 'set'.$name;
                $laboUpdate->$setter($value);
            }
        }
        $errors = $validator->validate($laboUpdate);
        if(count($errors)) {
            $errors = $serializer->serialize($errors, 'json');
            return new Response($errors, 500, [
                'Content-Type' => 'application/json'
            ]);
        }
        $entityManager->flush();
        $data = [
            'status' => 200,
            'message' => 'Le labo a bien ete mis a jour'
        ];
        return new JsonResponse($data);
    }

    public function deleteLabo(Laboratoire $labo, EntityManagerInterface $entityManager)
    {
        $entityManager->remove($labo);
        $entityManager->flush();
        return new Response(null, 204);
    }
}
