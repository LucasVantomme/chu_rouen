<?php

namespace App\Controller;

use App\Entity\Criticite;
use App\Repository\CriticiteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class CriticiteController extends AbstractController
{
    public function listCriticites(CriticiteRepository $criticiteRepository, SerializerInterface $serializer): JsonResponse
    {
        $criticites = $criticiteRepository->findAll();
        $json = $serializer->serialize(
            $criticites,
            'json',
            ['groups' => 'list_criticites']
        );
        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

    public function criticiteGetById(SerializerInterface $serializer, EntityManagerInterface $entityManager, Criticite $criticite): JsonResponse
    {
        $criticiteToGet = $entityManager->getRepository(Criticite::class)->find($criticite->getId());
        $json = $serializer->serialize(
            $criticiteToGet,
            'json',
            ['groups' => 'list_criticites']
        );
        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

    public function addCriticite(Request $request, SerializerInterface $serializer, EntityManagerInterface $entityManager, ValidatorInterface $validator){
        $criticite = $serializer->deserialize($request->getContent(), Criticite::class, 'json');
        $errors = $validator->validate($criticite);
        if(count($errors)) {
            $errors = $serializer->serialize($errors, 'json');
            return new Response($errors, 500, [
                'Content-Type' => 'application/json'
            ]);
        }
        $entityManager->persist($criticite);
        $entityManager->flush();
        $data = [
            'status' => 201,
            'message' => 'La criticte a bien ete ajoute'
        ];
        return new JsonResponse($data, 201);
    }

    public function updateCriticite(Request $request, SerializerInterface $serializer, Criticite $criticite, ValidatorInterface $validator, EntityManagerInterface $entityManager)
    {
        $criticiteUpdate = $entityManager->getRepository(Criticite::class)->find($criticite->getId());
        $data = json_decode($request->getContent());
        foreach ($data as $key => $value){
            if($key && !empty($value)) {
                $name = ucfirst($key);
                $setter = 'set'.$name;
                $criticiteUpdate->$setter($value);
            }
        }
        $errors = $validator->validate($criticiteUpdate);
        if(count($errors)) {
            $errors = $serializer->serialize($errors, 'json');
            return new Response($errors, 500, [
                'Content-Type' => 'application/json'
            ]);
        }
        $entityManager->flush();
        $data = [
            'status' => 200,
            'message' => 'La criticite a bien ete mis a jour'
        ];
        return new JsonResponse($data);
    }

    public function deleteCriticite(Criticite $criticite, EntityManagerInterface $entityManager)
    {
        $entityManager->remove($criticite);
        $entityManager->flush();
        return new Response(null, 204);
    }
}
