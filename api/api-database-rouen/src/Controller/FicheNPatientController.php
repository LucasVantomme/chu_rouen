<?php

namespace App\Controller;

use App\Entity\FicheNPatient;
use App\Entity\Utilisateurs;
use App\Entity\Cahiers;
use App\Repository\FicheNPatientRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use DateTime;

class FicheNPatientController extends AbstractController
{
    //----------------------- MODIF GREG ----------------------------
    public function listNFiches(FicheNPatientRepository $fichesRepository, SerializerInterface $serializer): JsonResponse
    {
        $fiches = $fichesRepository->findAll();
        
        $json = $serializer->serialize(
            $fiches,
            'json',
            ['groups' => ['list_Nfiches_principal',
              'list_cahiers_secondaire','list_utilisateurs_secondaire']]
        );
        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

    public function NficheGetById(SerializerInterface $serializer, EntityManagerInterface $entityManager, FicheNPatient $fiche): JsonResponse
    {
        $ficheToGet = $entityManager->getRepository(FicheNPatient::class)->find($fiche->getId());
        $json = $serializer->serialize(
            $ficheToGet,
            'json',
            ['groups' => ['list_Nfiches_principal',
              'list_cahiers_secondaire','list_utilisateurs_secondaire']]
        );
        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

    public function NficheGetByDateArchive(SerializerInterface $serializer, EntityManagerInterface $entityManager, $date): JsonResponse
    {
        $ficheToGet = $entityManager->getRepository(FicheNPatient::class)->findByDateArchived($date);
        $json = $serializer->serialize(
            $ficheToGet,
            'json',
            ['groups' => ['list_Nfiches_principal',
              'list_cahiers_secondaire','list_utilisateurs_secondaire']]
        );
        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

    public function addNFiche(Request $request, SerializerInterface $serializer, EntityManagerInterface $entityManager, ValidatorInterface $validator){
         //récuperation de l'id du createur
        $content =$request->getContent();
        $json = json_decode($content);
        $createur =  $entityManager->getRepository(Utilisateurs::class)->findByUserID($json->Createur->User_ID);
        
        //récuperation de l'id du cahier
        $cahier =  $entityManager->getRepository(Cahiers::class)->find($json->Cahier->id);

        $fiche = $serializer->deserialize($request->getContent(), FicheNPatient::class, 'json');
        $fiche->setCreateur($createur);
        $fiche->setCahier($cahier);
        $errors = $validator->validate($fiche);
        if(count($errors)) {
            $errors = $serializer->serialize($errors, 'json');
            return new Response($errors, 500, [
                'Content-Type' => 'application/json'
            ]);
        }
        $dateCreation = new DateTime();
        $dateCreation->format('Y-m-d');
        $fiche->setDateCreation($dateCreation);
        $entityManager->persist($fiche);
        $entityManager->flush();
        $data = [
            'status' => 201,
            'message' => 'Fiche non patient bien ajoute'
        ];
        return new JsonResponse($data, 201);
    }

    public function updateNFiche(Request $request, SerializerInterface $serializer, FicheNPatient $fiche, ValidatorInterface $validator, EntityManagerInterface $entityManager)
    {
        $ficheUpdate = $entityManager->getRepository(FicheNPatient::class)->find($fiche->getId());
        $data = json_decode($request->getContent());
        foreach ($data as $key => $value){
            $name = ucfirst($key);
            $setter = 'set'.$name;
            $ficheUpdate->$setter($value);
        }
        $errors = $validator->validate($ficheUpdate);
        if(count($errors)) {
            $errors = $serializer->serialize($errors, 'json');
            return new Response($errors, 500, [
                'Content-Type' => 'application/json'
            ]);
        }
        $entityManager->flush();
        $data = [
            'status' => 200,
            'message' => 'Fiche mis a jour'
        ];
        return new JsonResponse($data);
    }

    public function deleteNFiche(FicheNPatient $fiche, EntityManagerInterface $entityManager)
    {
        $entityManager->remove($fiche);
        $entityManager->flush();
        return new Response(null, 204);
    }
}