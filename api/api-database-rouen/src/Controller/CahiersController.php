<?php

namespace App\Controller;

use App\Entity\Cahiers;
use App\Entity\FichePatient;
use App\Entity\Utilisateurs;
use App\Repository\CahiersRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Id;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CahiersController extends AbstractController
{
    public function listCahiers(CahiersRepository $cahiersRepository, SerializerInterface $serializer): JsonResponse
    {
        $cahiers = $cahiersRepository->findAll();
        
        $json = $serializer->serialize(
            $cahiers,
            'json',
            ['groups' => ['list_cahiers_principal',
              'list_fiches_secondaire','list_Nfiches_secondaire','list_utilisateurs_secondaire']]
        );
        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

    public function cahierGetById(SerializerInterface $serializer, EntityManagerInterface $entityManager, Cahiers $cahier): JsonResponse
    {
        $cahierToGet = $entityManager->getRepository(Cahiers::class)->find($cahier->getId());

        $json = $serializer->serialize(
            $cahierToGet,
            'json',
            ['groups' => ['list_cahiers_principal',
              'list_fiches_secondaire','list_Nfiches_secondaire','list_utilisateurs_secondaire']]
        );
        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

    public function cahierGetByDateArchive(SerializerInterface $serializer, EntityManagerInterface $entityManager, $date): JsonResponse
    {
        $cahiersToGet = $entityManager->getRepository(Cahiers::class)->findByDateArchived($date);
        $json = $serializer->serialize(
            $cahiersToGet,
            'json',
            ['groups' => ['list_Nfiches_principal',
              'list_cahiers_secondaire','list_utilisateurs_secondaire']]
        );
        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

    public function addCahier(Request $request, SerializerInterface $serializer, EntityManagerInterface $entityManager, ValidatorInterface $validator){
        //récuperation de l'id du createur
        $content =$request->getContent();
        $json = json_decode($content);
        $createur =  $entityManager->getRepository(Utilisateurs::class)->findByUserID($json->Createur->User_ID);

        //get du contenu general
        $cahier = $serializer->deserialize($content, Cahiers::class, 'json');
        $cahier->setCreateur($createur);
        $errors = $validator->validate($cahier);
        if(count($errors)) {
            $errors = $serializer->serialize($errors, 'json');
            return new Response($errors, 500, [
                'Content-Type' => 'application/json'
            ]);
        }

        //ajout du cahier sans persist sur le createur pour l'ajout
        $entityManager->persist($cahier);
        $entityManager->flush();
        $data = [
            'status' => 201,
            'message' => 'Cahier bien ajoute'
        ];
        return new JsonResponse($data, 201);
    }

    public function updateCahier(Request $request, SerializerInterface $serializer, Cahiers $cahier, ValidatorInterface $validator, EntityManagerInterface $entityManager)
    {
        $cahierUpdate = $entityManager->getRepository(Cahiers::class)->find($cahier->getId());
        $data = json_decode($request->getContent());
        foreach ($data as $key => $value){
            if($key && !empty($value)) {
                $name = ucfirst($key);
                $setter = 'set'.$name;
                $cahierUpdate->$setter($value);
            }
        }
        $errors = $validator->validate($cahierUpdate);
        if(count($errors)) {
            $errors = $serializer->serialize($errors, 'json');
            return new Response($errors, 500, [
                'Content-Type' => 'application/json'
            ]);
        }
        $entityManager->flush();
        $data = [
            'status' => 200,
            'message' => 'Cahier mis a jour'
        ];
        return new JsonResponse($data);
    }

    public function deleteCahier(Cahiers $cahier, EntityManagerInterface $entityManager)
    {
        $entityManager->remove($cahier);
        $entityManager->flush();
        return new Response(null, 204);
    }

    public function cahierGetCreateur(SerializerInterface $serializer, EntityManagerInterface $entityManager, Cahiers $cahier): JsonResponse
    {
        $cahierToGet = $entityManager->getRepository(Cahiers::class)->find($cahier->getId());

        $json = $serializer->serialize(
            $cahierToGet,
            'json',
            ['groups' => ['createur']]
        );
        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

    public function listCahiersRoles(SerializerInterface $serializer, EntityManagerInterface $entityManager, Utilisateurs $utilisateur): JsonResponse
    {
        $utilisateurWithRoles = $entityManager->getRepository(Utilisateurs::class)->find($utilisateur->getId());
        $roles = $utilisateurWithRoles->getRoles();
        $cahierToGet = [];
        foreach($roles as $role){
            if ($role->getIsActive()) {
                $cahiers = $entityManager->getRepository(Cahiers::class)->findByRoles($role->getTitre());
                foreach($cahiers as $cahier){
                    array_push($cahierToGet,$cahier);
                }
            }
        }
        $json = $serializer->serialize(
            $cahierToGet,
            'json',
            ['groups' => ['list_cahiers_principal',
              'list_fiches_secondaire','list_Nfiches_secondaire','list_utilisateurs_secondaire']]
        );
        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }
}
