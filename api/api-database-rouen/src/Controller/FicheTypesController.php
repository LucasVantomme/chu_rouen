<?php

namespace App\Controller;

use App\Entity\FicheTypes;
use App\Repository\FicheTypesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class FicheTypesController extends AbstractController
{
    public function listFichesTypes(FicheTypesRepository $fichetypesRepository, SerializerInterface $serializer): JsonResponse
    {
        $fichetypes = $fichetypesRepository->findAll();
        $json = $serializer->serialize(
            $fichetypes,
            'json',
            ['groups' => 'list_fichetypes']
        );
        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

    public function ficheTypeGetById(SerializerInterface $serializer, EntityManagerInterface $entityManager, FicheTypes $fichetypes): JsonResponse
    {
        $ficheTypesToGet = $entityManager->getRepository(FicheTypes::class)->find($fichetypes->getId());
        $json = $serializer->serialize(
            $ficheTypesToGet,
            'json',
            ['groups' => 'list_fichetypes']
        );
        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

    public function addFicheType(Request $request, SerializerInterface $serializer, EntityManagerInterface $entityManager, ValidatorInterface $validator){
        $fichetypes = $serializer->deserialize($request->getContent(), FicheTypes::class, 'json');
        $errors = $validator->validate($fichetypes);
        if(count($errors)) {
            $errors = $serializer->serialize($errors, 'json');
            return new Response($errors, 500, [
                'Content-Type' => 'application/json'
            ]);
        }
        $entityManager->persist($fichetypes);
        $entityManager->flush();
        $data = [
            'status' => 201,
            'message' => 'Le type a bien ete ajoute'
        ];
        return new JsonResponse($data, 201);
    }

    public function updateFicheType(Request $request, SerializerInterface $serializer, FicheTypes $fichetypes, ValidatorInterface $validator, EntityManagerInterface $entityManager)
    {
        $ficheTypesUpdate = $entityManager->getRepository(FicheTypes::class)->find($fichetypes->getId());
        $data = json_decode($request->getContent());
        foreach ($data as $key => $value){
            if($key && !empty($value)) {
                $name = ucfirst($key);
                $setter = 'set'.$name;
                $ficheTypesUpdate->$setter($value);
            }
        }
        $errors = $validator->validate($ficheTypesUpdate);
        if(count($errors)) {
            $errors = $serializer->serialize($errors, 'json');
            return new Response($errors, 500, [
                'Content-Type' => 'application/json'
            ]);
        }
        $entityManager->flush();
        $data = [
            'status' => 200,
            'message' => 'Le type a bien ete mis a jour'
        ];
        return new JsonResponse($data);
    }

    public function deleteFicheType(FicheTypes $fichetypes, EntityManagerInterface $entityManager)
    {
        $entityManager->remove($fichetypes);
        $entityManager->flush();
        return new Response(null, 204);
    }
}
