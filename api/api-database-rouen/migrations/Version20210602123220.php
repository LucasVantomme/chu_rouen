<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210602123220 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cahiers ADD is_active TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE fiche_npatient CHANGE criticite criticite VARCHAR(255) DEFAULT NULL, CHANGE texte_libre texte_libre VARCHAR(255) DEFAULT NULL, CHANGE description description VARCHAR(255) DEFAULT NULL, CHANGE conclusion conclusion VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE fiche_patient CHANGE description description VARCHAR(255) DEFAULT NULL, CHANGE conclusion conclusion VARCHAR(255) DEFAULT NULL, CHANGE criticite criticite VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE roles ADD is_active TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE utilisateurs ADD is_active TINYINT(1) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cahiers DROP is_active');
        $this->addSql('ALTER TABLE fiche_npatient CHANGE criticite criticite VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE conclusion conclusion VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE texte_libre texte_libre VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE fiche_patient CHANGE description description VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE criticite criticite VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE conclusion conclusion VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE roles DROP is_active');
        $this->addSql('ALTER TABLE utilisateurs DROP is_active');
    }
}
