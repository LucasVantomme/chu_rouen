<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210607152957 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE utilisateurs_roles');
        $this->addSql('ALTER TABLE roles ADD utilisateurs_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE roles ADD CONSTRAINT FK_B63E2EC71E969C5 FOREIGN KEY (utilisateurs_id) REFERENCES utilisateurs (id)');
        $this->addSql('CREATE INDEX IDX_B63E2EC71E969C5 ON roles (utilisateurs_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE utilisateurs_roles (utilisateurs_id INT NOT NULL, roles_id INT NOT NULL, INDEX IDX_55B6AB3838C751C4 (roles_id), INDEX IDX_55B6AB381E969C5 (utilisateurs_id), PRIMARY KEY(utilisateurs_id, roles_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE utilisateurs_roles ADD CONSTRAINT FK_55B6AB381E969C5 FOREIGN KEY (utilisateurs_id) REFERENCES utilisateurs (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE utilisateurs_roles ADD CONSTRAINT FK_55B6AB3838C751C4 FOREIGN KEY (roles_id) REFERENCES roles (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE roles DROP FOREIGN KEY FK_B63E2EC71E969C5');
        $this->addSql('DROP INDEX IDX_B63E2EC71E969C5 ON roles');
        $this->addSql('ALTER TABLE roles DROP utilisateurs_id');
    }
}
