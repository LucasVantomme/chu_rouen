<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210518085734 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE cahiers (id INT AUTO_INCREMENT NOT NULL, createur_id INT DEFAULT NULL, date_creation DATETIME NOT NULL, type VARCHAR(255) NOT NULL, rattachement VARCHAR(255) NOT NULL, is_archived TINYINT(1) NOT NULL, date_archive DATETIME DEFAULT NULL, nom VARCHAR(255) NOT NULL, INDEX IDX_91EFE3C573A201E5 (createur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE criticite (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fiche_npatient (id INT AUTO_INCREMENT NOT NULL, createur_id INT NOT NULL, cahier_id INT NOT NULL, titre VARCHAR(255) NOT NULL, date_creation DATETIME NOT NULL, secteur VARCHAR(255) NOT NULL, criticite VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, texte_libre VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, conclusion VARCHAR(255) NOT NULL, is_archived TINYINT(1) NOT NULL, date_archive DATETIME DEFAULT NULL, INDEX IDX_41EB5FE473A201E5 (createur_id), INDEX IDX_41EB5FE41E0D9F2 (cahier_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fiche_patient (id INT AUTO_INCREMENT NOT NULL, createur_id INT NOT NULL, cahier_id INT NOT NULL, titre VARCHAR(255) NOT NULL, date_creation DATETIME NOT NULL, secteur VARCHAR(255) NOT NULL, nom_patient VARCHAR(255) NOT NULL, date_naissance DATETIME NOT NULL, num_dossier INT NOT NULL, description VARCHAR(255) NOT NULL, conclusion VARCHAR(255) NOT NULL, is_archived TINYINT(1) NOT NULL, date_archive DATETIME DEFAULT NULL, criticite VARCHAR(255) NOT NULL, INDEX IDX_2DB8C3173A201E5 (createur_id), INDEX IDX_2DB8C311E0D9F2 (cahier_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE laboratoire (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE roles (id INT AUTO_INCREMENT NOT NULL, utilisateurs_id INT DEFAULT NULL, titre VARCHAR(255) NOT NULL, INDEX IDX_B63E2EC71E969C5 (utilisateurs_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE utilisateurs (id INT AUTO_INCREMENT NOT NULL, role_id INT DEFAULT NULL, password VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, date_naissance DATETIME NOT NULL, telephone VARCHAR(255) NOT NULL, labo VARCHAR(255) NOT NULL, user_id VARCHAR(255) NOT NULL, INDEX IDX_497B315ED60322AC (role_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE cahiers ADD CONSTRAINT FK_91EFE3C573A201E5 FOREIGN KEY (createur_id) REFERENCES utilisateurs (id)');
        $this->addSql('ALTER TABLE fiche_npatient ADD CONSTRAINT FK_41EB5FE473A201E5 FOREIGN KEY (createur_id) REFERENCES utilisateurs (id)');
        $this->addSql('ALTER TABLE fiche_npatient ADD CONSTRAINT FK_41EB5FE41E0D9F2 FOREIGN KEY (cahier_id) REFERENCES cahiers (id)');
        $this->addSql('ALTER TABLE fiche_patient ADD CONSTRAINT FK_2DB8C3173A201E5 FOREIGN KEY (createur_id) REFERENCES utilisateurs (id)');
        $this->addSql('ALTER TABLE fiche_patient ADD CONSTRAINT FK_2DB8C311E0D9F2 FOREIGN KEY (cahier_id) REFERENCES cahiers (id)');
        $this->addSql('ALTER TABLE roles ADD CONSTRAINT FK_B63E2EC71E969C5 FOREIGN KEY (utilisateurs_id) REFERENCES utilisateurs (id)');
        $this->addSql('ALTER TABLE utilisateurs ADD CONSTRAINT FK_497B315ED60322AC FOREIGN KEY (role_id) REFERENCES roles (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE fiche_npatient DROP FOREIGN KEY FK_41EB5FE41E0D9F2');
        $this->addSql('ALTER TABLE fiche_patient DROP FOREIGN KEY FK_2DB8C311E0D9F2');
        $this->addSql('ALTER TABLE utilisateurs DROP FOREIGN KEY FK_497B315ED60322AC');
        $this->addSql('ALTER TABLE cahiers DROP FOREIGN KEY FK_91EFE3C573A201E5');
        $this->addSql('ALTER TABLE fiche_npatient DROP FOREIGN KEY FK_41EB5FE473A201E5');
        $this->addSql('ALTER TABLE fiche_patient DROP FOREIGN KEY FK_2DB8C3173A201E5');
        $this->addSql('ALTER TABLE roles DROP FOREIGN KEY FK_B63E2EC71E969C5');
        $this->addSql('DROP TABLE cahiers');
        $this->addSql('DROP TABLE criticite');
        $this->addSql('DROP TABLE fiche_npatient');
        $this->addSql('DROP TABLE fiche_patient');
        $this->addSql('DROP TABLE laboratoire');
        $this->addSql('DROP TABLE roles');
        $this->addSql('DROP TABLE utilisateurs');
    }
}
