<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210607154123 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE roles_utilisateurs (roles_id INT NOT NULL, utilisateurs_id INT NOT NULL, INDEX IDX_7B40729138C751C4 (roles_id), INDEX IDX_7B4072911E969C5 (utilisateurs_id), PRIMARY KEY(roles_id, utilisateurs_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE roles_utilisateurs ADD CONSTRAINT FK_7B40729138C751C4 FOREIGN KEY (roles_id) REFERENCES roles (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE roles_utilisateurs ADD CONSTRAINT FK_7B4072911E969C5 FOREIGN KEY (utilisateurs_id) REFERENCES utilisateurs (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE roles DROP FOREIGN KEY FK_B63E2EC71E969C5');
        $this->addSql('DROP INDEX IDX_B63E2EC71E969C5 ON roles');
        $this->addSql('ALTER TABLE roles DROP utilisateurs_id');
        $this->addSql('ALTER TABLE utilisateurs DROP FOREIGN KEY FK_497B315E38C751C4');
        $this->addSql('DROP INDEX IDX_497B315E38C751C4 ON utilisateurs');
        $this->addSql('ALTER TABLE utilisateurs DROP roles_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE roles_utilisateurs');
        $this->addSql('ALTER TABLE roles ADD utilisateurs_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE roles ADD CONSTRAINT FK_B63E2EC71E969C5 FOREIGN KEY (utilisateurs_id) REFERENCES utilisateurs (id)');
        $this->addSql('CREATE INDEX IDX_B63E2EC71E969C5 ON roles (utilisateurs_id)');
        $this->addSql('ALTER TABLE utilisateurs ADD roles_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE utilisateurs ADD CONSTRAINT FK_497B315E38C751C4 FOREIGN KEY (roles_id) REFERENCES roles (id)');
        $this->addSql('CREATE INDEX IDX_497B315E38C751C4 ON utilisateurs (roles_id)');
    }
}
