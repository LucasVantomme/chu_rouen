<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210608153658 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE utilisateurs_roles (utilisateurs_id INT NOT NULL, roles_id INT NOT NULL, INDEX IDX_55B6AB381E969C5 (utilisateurs_id), INDEX IDX_55B6AB3838C751C4 (roles_id), PRIMARY KEY(utilisateurs_id, roles_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE utilisateurs_roles ADD CONSTRAINT FK_55B6AB381E969C5 FOREIGN KEY (utilisateurs_id) REFERENCES utilisateurs (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE utilisateurs_roles ADD CONSTRAINT FK_55B6AB3838C751C4 FOREIGN KEY (roles_id) REFERENCES roles (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE roles_utilisateurs');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE roles_utilisateurs (roles_id INT NOT NULL, utilisateurs_id INT NOT NULL, INDEX IDX_7B4072911E969C5 (utilisateurs_id), INDEX IDX_7B40729138C751C4 (roles_id), PRIMARY KEY(roles_id, utilisateurs_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE roles_utilisateurs ADD CONSTRAINT FK_7B4072911E969C5 FOREIGN KEY (utilisateurs_id) REFERENCES utilisateurs (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE roles_utilisateurs ADD CONSTRAINT FK_7B40729138C751C4 FOREIGN KEY (roles_id) REFERENCES roles (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE utilisateurs_roles');
    }
}
