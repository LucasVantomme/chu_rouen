import { BrowserModule } from "@angular/platform-browser";
import { registerLocaleData } from "@angular/common";
import fr from "@angular/common/locales/fr";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AvatarModule } from "primeng/avatar";
import { NZ_I18N, fr_FR } from "ng-zorro-antd/i18n";
import {
    NzSpinModule,
    NzFormModule,
    NzCardModule,
    NzInputModule,
    NzButtonModule,
    NzDropDownModule,
    NzIconModule,
    NzAlertModule
} from "ng-zorro-antd";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HomeComponent } from "./home/home.component";
import { StatisticsComponent } from "./statistics/statistics.component";
import { CardModule } from "./card/card.module";
import { BookModule } from "./book/book.module";
import { AuthComponent } from "./auth/auth.component";
import { AdministrationModule } from "./administration/administration.module";

registerLocaleData(fr);

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        StatisticsComponent,
        AuthComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        HttpClientModule,
        FormsModule,
        AppRoutingModule,
        BookModule,
        CardModule,
        AdministrationModule,
        AvatarModule,
        NzSpinModule,
        NzCardModule,
        NzFormModule,
        NzInputModule,
        NzButtonModule,
        NzDropDownModule,
        NzIconModule,
        NzAlertModule
    ],
    providers: [
        { provide: NZ_I18N, useValue: fr_FR }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
