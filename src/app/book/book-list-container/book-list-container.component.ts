import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { Router } from "@angular/router";
import { finalize } from "rxjs/operators";

import { Book } from "../@models/book.model";
import { BookService } from "../@core/book.service";
import { BookState } from "../@core/book.state";
import { AuthState } from "src/app/@core/auth/auth.state";
import { User } from "src/app/auth/@model/user.model";

@Component({
    selector: "app-book-list-container",
    templateUrl: "./book-list-container.component.html",
    styleUrls: ["./book-list-container.component.scss"]
})
export class BookListContainerComponent implements OnInit {
    /**
     * List of books
     */
    books: Book[] = [];
    /**
     * Is loading
     */
    isLoading: boolean = false;
    /**
     * All books get from API
     */
    private allBooks: Book[];
    private user: User;

    /**
     * Book list container component
     * @param location Location
     * @param bookService Book Service
     * @param router Router
     * @param bookSate Book State
     */
    constructor(
        private readonly location: Location,
        private readonly bookService: BookService,
        private readonly router: Router,
        private readonly bookState: BookState,
        private readonly authState: AuthState
    ) { }

    /**
     * Angular cycle OnInit
     */
    ngOnInit() {
        this.isLoading = true;
        this.authState.getUser()
        .subscribe((user: User) => this.user = user);

        this.bookService.getBooks(this.user)
        .pipe(
            finalize(() => this.isLoading = false)
        )
        .subscribe((books: Book[]) =>  {
            this.books = books;
            this.allBooks = books;
        });
    }

    /**
     * Back button clicked
     */
    backClicked() {
        this.location.back();
    }

    /**
     * Search book
     * @param search search input
     */
    searchBook(search: string): void {
        const books = this.allBooks.filter((book: Book) => book.name.includes(search));
        this.books = books;
    }

    /**
     * When a book is selected
     * @param book selected Book
     */
    selectedBook(book: Book): void {
        this.bookState.setSelectedBook(book);
        this.router.navigate(["/cards"]);
    }
}
