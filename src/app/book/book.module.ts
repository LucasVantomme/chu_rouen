import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { NzCardModule } from "ng-zorro-antd/card";
import { NzTableModule } from "ng-zorro-antd/table";
import { NzButtonModule } from "ng-zorro-antd/button";
import { NzInputModule } from "ng-zorro-antd/input";
import { NzIconModule } from "ng-zorro-antd/icon";
import { NzToolTipModule } from "ng-zorro-antd/tooltip";
import { NzNotificationModule } from "ng-zorro-antd/notification";
import { NzBadgeModule } from "ng-zorro-antd/badge";
import { NzPaginationModule } from "ng-zorro-antd/pagination";
import { NzSelectModule } from "ng-zorro-antd";

import { AppRoutingModule } from "../app-routing.module";
import { BookFormComponent } from "./book-form/book-form.component";
import { BookListComponent } from "./book-list/book-list.component";
import { BookListContainerComponent } from "./book-list-container/book-list-container.component";

@NgModule({
    declarations: [
        BookFormComponent,
        BookListComponent,
        BookListContainerComponent
    ],
    imports: [
        CommonModule,
        BrowserModule,
        ReactiveFormsModule,
        FormsModule,
        AppRoutingModule,
        NzCardModule,
        NzTableModule,
        NzButtonModule,
        NzInputModule,
        NzIconModule,
        NzToolTipModule,
        NzNotificationModule,
        NzBadgeModule,
        NzPaginationModule,
        NzSelectModule
    ]
})
export class BookModule { }
