import { Card } from "../../card/@models/card.model";

/**
 * Book model
 */
export class Book {
    /**
     * Book's id
     */
    id: number;
    /**
     * Book's name
     */
    name: string;
    /**
     * Book's group
     */
    group: string;
    /**
     * Book's type
     */
    type: string;
    /**
     * Patient cards
     */
    patientCards: Card[];
    /**
     * No patient cards
     */
    noPatientCards: Card[];

    constructor(name: string, group: string, type: string, patientCards?: Card[], noPatientCards?: Card[], id?: number) {
        this.id = id;
        this.name = name;
        this.group = group;
        this.type = type;
        this.patientCards = patientCards;
        this.noPatientCards = noPatientCards;
    }
}
