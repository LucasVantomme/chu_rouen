import { HttpClient } from "@angular/common/http";
import { async, TestBed } from "@angular/core/testing";

import { BookService } from "./book.service";

describe("BookService", () => {
  beforeEach(async(() => {
        TestBed.configureTestingModule({
        imports: [
            HttpClient,
        ],
        declarations: [
            BookService
        ],
        }).compileComponents();
    }));

  it("should be created", () => {
    const service: BookService = TestBed.get(BookService);
    expect(service).toBeTruthy();
  });
});
