import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";

import { Book } from "../@models/book.model";

@Injectable({
    providedIn: "root"
})
export class BookState {
    /**
     * selected Book
     */
    private readonly selectedBook = new BehaviorSubject<Book>(null);

    constructor() { }

    /**
     * Set the selected Book
     * @param book selected Book
     */
    setSelectedBook(book: Book): void {
        this.selectedBook.next(book);
    }

    /**
     * Get Selected book
     */
    getSelectedBook(): Observable<Book> {
        return this.selectedBook;
    }
}
