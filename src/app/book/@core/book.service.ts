import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { finalize, map } from "rxjs/operators";
import { User } from "src/app/auth/@model/user.model";

import { LoaderState } from "../../@core/loader/loader.state";
import { NoPatientCard } from "../../card/@models/non-patient-card.model";
import { PatientCard } from "../../card/@models/patient-card.model";
import { Book } from "../@models/book.model";

/**
 * Book service
 */
@Injectable({
    providedIn: "root"
})
export class BookService {

    /**
     * Book service's constructor
     * @param http Http Client
     * @param loaderState Loader State
     */
    constructor(
        private readonly http: HttpClient,
        private readonly loaderState: LoaderState
    ) { }

    /**
     * Get Book by id
     * @param id id book
     */
    getBookById(id: number): Observable<Book> {
        this.loaderState.setLoader(true);
        return this.http.get(`/api/cahier/${id}`)
        .pipe(map((cahier: any) => new Book(
            cahier.Nom, cahier.Rattachement, cahier.Type, this.setPatientCard(cahier), this.setNoPatientCard(cahier), cahier.id
        )),
        finalize(() => this.loaderState.setLoader(false)));
    }

    /**
     * Get books
     */
    getBooks(user: User): Observable<Book[]> {
        this.loaderState.setLoader(true);
        return this.http.get(`/api/listcahiersrole/${user.idDelete}`)
        .pipe(map((cahiers: any[]) => {
            return cahiers.map((cahier: any) => new Book(
                cahier.Nom, cahier.Rattachement, cahier.Type, this.setPatientCard(cahier), this.setNoPatientCard(cahier), cahier.id
            ));
            }),
        finalize(() => this.loaderState.setLoader(false)));
    }

    /**
     * Add book
     * @param book Book
     */
    addBook(book: Book, user: User): Observable<Book> {
        this.loaderState.setLoader(true);
        const bookToAdd = {
            Type: book.type,
            DateCreation: new Date(),
            Rattachement: book.group,
            Nom: book.name,
            Createur: {
                User_ID: user.id
            }
        };
        return this.http.post("/api/addcahier", bookToAdd)
        .pipe(map((cahier: any) => new Book(
            cahier.Nom,
            cahier.Rattachement,
            cahier.Type,
            cahier.fichesPatient,
            cahier.fichesNPatient,
            cahier.id
        )),
        finalize(() => this.loaderState.setLoader(false)));
    }

    /**
     * Set Patient card
     * @param card Card
     */
    private setPatientCard(card: any): PatientCard[] {
        return card.fichesPatient.map((fiche: any) => new PatientCard(
            fiche.Titre,
            fiche.isArchived,
            fiche.NomPatient,
            fiche.Secteur,
            fiche.NumDossier,
            fiche.DateNaissance,
            fiche.Description,
            fiche.Conclusion,
            fiche.DateCreation,
            fiche.criticite,
            fiche.Createur,
            fiche.id,
            fiche.DateArchive
        ));
    }

    /**
     * Set Non patient card
     * @param card card
     */
    private setNoPatientCard(card: any): PatientCard[] {
        return card.fichesNPatient.map((fiche: any) => new NoPatientCard(
            fiche.Titre,
            fiche.isArchived,
            fiche.Type,
            fiche.Secteur,
            fiche.TexteLibre,
            fiche.Description,
            fiche.Conclusion,
            fiche.DateCreation,
            fiche.Criticite,
            fiche.Createur,
            fiche.id,
            fiche.DateArchive
        ));
    }
}
