import { Component, Input, Output, EventEmitter, OnChanges, OnInit } from "@angular/core";
import { AuthState } from "src/app/@core/auth/auth.state";
import { Role } from "src/app/auth/@model/role.model";
import { User } from "src/app/auth/@model/user.model";
import { NoPatientCard } from "src/app/card/@models/non-patient-card.model";
import { PatientCard } from "src/app/card/@models/patient-card.model";

import { Book } from "../@models/book.model";

@Component({
    selector: "app-book-list",
    templateUrl: "./book-list.component.html",
    styleUrls: ["./book-list.component.scss"]
})
export class BookListComponent implements OnChanges, OnInit {
    /**
     * Is loading
     */
    @Input() isLoading: boolean;
    /**
     * Book list
     */
    @Input() books: Book[];
    /**
     * Emit page return
     */
    @Output() backPage: EventEmitter<boolean> = new EventEmitter();
    /**
     * Emit search return
     */
    @Output() searchingBook: EventEmitter<string> = new EventEmitter();
    /**
     * Emit selected book
     */
    @Output() selectedBook: EventEmitter<Book> = new EventEmitter();
    /**
     * Search input
     */
    search: string = "";
    /**
     * Books to display
     */
    booksToDisplay: Book[];
    private user: User;

    /**
     * Book list component's constructor
     */
    constructor(
        private readonly authState: AuthState
    ) { }

    ngOnInit(): void {
        this.authState.getUser()
        .subscribe((user: User) => this.user = user);
    }

    /**
     * Angular Cycle OnInit
     */
    ngOnChanges(): void {
        this.booksToDisplay = this.books.slice(0, 8);
    }

    /**
     * Back button clicked
     */
    backClicked() {
        this.backPage.emit(true);
    }

    /**
     * Search book
     */
    searchBook(): void {
        this.searchingBook.emit(this.search);
    }

    /**
     * Book is selected from the list
     * @param book Book
     */
    bookSelected(book: Book): void {
        this.selectedBook.emit(book);
    }

    /**
     * Get length of books
     */
    getBookLength(): number {
        return this.books.length;
    }

    /**
     * Get number of cards in this book
     * @param book book
     */
    getCardsLength(book: Book): number {
        let summ = 0;

        book.patientCards.forEach((card: PatientCard) => {
            if (new Date(card.creationDate).getTime() > new Date(this.user.lastConnection).getTime()) {
                summ = summ + 1;
            }
        });

        book.noPatientCards.forEach((card: NoPatientCard) => {
            if (new Date(card.creationDate).getTime() > new Date(this.user.lastConnection).getTime()) {
                summ = summ + 1;
            }
        });
        return summ;
    }

    /**
     * Change books to display
     * @param page page number
     */
    changeBooks(page: number): void {
        if (page > 1) {
            this.booksToDisplay = this.books.slice(8 * (page - 1 ), 8 * page);
        } else {
            this.booksToDisplay = this.books.slice(0, 8);
        }
    }

    isAdmin(): boolean {
        let isAdministrateur: boolean = false;

        this.user.roles.forEach((role: any) => {
            if (role.Titre === "Administrateur") {
                isAdministrateur = true;
            }
        });

        return isAdministrateur;
    }
}
