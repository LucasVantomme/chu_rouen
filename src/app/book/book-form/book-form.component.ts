import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NzNotificationService } from "ng-zorro-antd";

import { BookService } from "../@core/book.service";
import { Book } from "../@models/book.model";
import { Router } from "@angular/router";
import { AuthState } from "src/app/@core/auth/auth.state";
import { User } from "src/app/auth/@model/user.model";
import { RoleService } from "src/app/administration/@core/role.service";
import { Role } from "src/app/auth/@model/role.model";

@Component({
    selector: "app-book-form",
    templateUrl: "./book-form.component.html",
    styleUrls: ["./book-form.component.scss"]
})
export class BookFormComponent implements OnInit {
    /**
     * book Form
     */
    bookForm: FormGroup;
    /**
     * All groups for a book
     */
    groups: Role[] = [];

    private user: User;

    /**
     * Constructor book form component
     * @param formBuilder Form Builder
     * @param location Location
     * @param bookService Book Service,
     * @param notification Notification Service from Ant Design
     * @param router Router
     */
    constructor(
        private readonly formBuilder: FormBuilder,
        private readonly location: Location,
        private readonly bookService: BookService,
        private readonly notification: NzNotificationService,
        private readonly router: Router,
        private readonly authState: AuthState,
        private readonly roleService: RoleService
    ) { }

    /**
     * Angular Cycle OnInit
     */
    ngOnInit() {
        this.authState.getUser()
        .subscribe((user: User) => this.user = user);

        this.bookForm = this.formBuilder.group({
            name: [null, [Validators.required]],
            group: [null, [Validators.required]],
            type: [null, [Validators.required]],
        });

        this.roleService.getRoles()
        .subscribe((roles: Role[]) => {
            this.groups = roles;
        });
    }

    /**
     * Back button clicked
     */
    backClicked() {
        this.location.back();
    }

    /**
     * Add new book
     */
    addNewBook(): void {
        if (this.bookForm.get("name").value && this.bookForm.get("group").value && this.bookForm.get("type").value) {
            const book = new Book(this.bookForm.get("name").value, this.bookForm.get("group").value, this.bookForm.get("type").value);
            this.bookService.addBook(book, this.user)
            .subscribe((bookCreated: Book) => {
                this.notification.success("Nouveau cahier enregistré", `Le cahier ${book.name} est bien enregistré`);
                this.router.navigate(["/books"]);
            });
        } else {
            console.log("NOPEEEEEEEEEE");
        }
    }
}
