import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Router } from "@angular/router";
import { NzNotificationService } from "ng-zorro-antd";

import { LoaderState } from "../../@core/loader/loader.state";
import { Book } from "../../book/@models/book.model";
import { CardService } from "../@core/card.service";
import { CardState } from "../@core/card.state";
import { Card } from "../@models/card.model";
import { NoPatientCard } from "../@models/non-patient-card.model";
import { PatientCard } from "../@models/patient-card.model";

@Component({
    selector: "app-card-list",
    templateUrl: "./card-list.component.html",
    styleUrls: ["./card-list.component.scss"]
})
export class CardListComponent {
    /**
     * Selected Book
     */
    @Input() selectedBook: Book;
    /**
     * Cards of selected book
     */
    @Input() cards: Card[];
    /**
     * Emit page return
     */
    @Output() backPage: EventEmitter<boolean> = new EventEmitter();
    /**
     * Emit search return
     */
    @Output() searchingCard: EventEmitter<string> = new EventEmitter();
    /**
     * Emit if we have to include archived cards
     */
     @Output() includeArchivedCards: EventEmitter<boolean> = new EventEmitter();
    /**
     * Search input
     */
    search: string = "";
    /**
     * Include archived cards
     */
    includeArchived: boolean = false;

    /**
     * Card List component's constructor
     * @param cardState Carc State
     * @param router Router
     * @param cardService Card Service
     */
    constructor(
        private readonly cardState: CardState,
        private readonly router: Router,
        private readonly cardService: CardService,
        private readonly loaderState: LoaderState,
        private readonly notification: NzNotificationService
    ) { }

    /**
     * Card is clicked
     * @param card Card
     */
    cardClicked(card: Card): void {
        this.cardState.setSelectedCard(card);
        this.router.navigate(["/card"]);
    }

    /**
     * Back button clicked
     */
    backClicked() {
        this.backPage.emit(true);
    }

    /**
     * Create a card
     * @param type Type of card to create
     */
    createCard(type: string): void {
        this.cardState.setSelectedTypeCard(type);
        this.router.navigate(["/cards/new"]);
    }

    /**
     * Navigate to create a report
     */
    reportClicked(): void {
        this.router.navigate(["/cards/report"]);
    }

    /**
     * Search card
     */
    searchCard(): void {
        this.searchingCard.emit(this.search);
    }

    /**
     * Map card to archive
     * @param card card
     */
    selectCard(card: PatientCard|NoPatientCard): void {
        card.archiveDate = (card.archiveDate) ? null : new Date().toISOString();
    }

    /**
     * Archive list of selected cards
     */
    archive(): void {
        this.loaderState.setLoader(true);
        this.cards.forEach((card: PatientCard|NoPatientCard) => {
            if (card.archiveDate && !card.isArchived && card instanceof PatientCard) {
                this.cardService.archivePatientCard(card)
                .subscribe((archivedCard: PatientCard) => {
                    this.notification.success("Fiche archivée", `La fiche  ${card.title} est bien archivée`);
                    this.backClicked();
                });
            } else if (card.archiveDate && !card.isArchived && card instanceof NoPatientCard) {
                this.cardService.archiveObjectCard(card)
                .subscribe((archivedCard: PatientCard) => {
                    this.notification.success("Fiche archivée", `La fiche  ${card.title} est bien archivée`);
                    this.backClicked();
                });
            }
        });
        this.loaderState.setLoader(false);
    }

    /**
     * Set if we need to include archived cards or not
     * @param archived archived
     */
    changeIncludeArchivedCards(archived: boolean): void {
        this.includeArchivedCards.emit(archived);
        this.includeArchived = !this.includeArchived;
    }
}
