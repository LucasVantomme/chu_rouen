import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { finalize, map } from "rxjs/operators";
import { combineLatest } from "rxjs";

import { CardService } from "../@core/card.service";
import { NoPatientCard } from "../@models/non-patient-card.model";
import { PatientCard } from "../@models/patient-card.model";
import { LoaderState } from "../../@core/loader/loader.state";
import { User } from "src/app/auth/@model/user.model";
import { UserService } from "src/app/administration/@core/user.service";

@Component({
    selector: "app-card-report",
    templateUrl: "./card-report.component.html",
    styleUrls: ["./card-report.component.scss"]
})
export class CardReportComponent implements OnInit {
    /**
     * selected date for the report
     */
    selectedDate: Date = null;
    /**
     * Archived cards
     */
    cards: any[] = [];
    listOfUsers: User[];
    listOfSelectedValue: string[] = [];

    /**
     * Card report's component constructor
     * @param location Location
     * @param cardService Card Service
     * @param loaderState Loader State
     */
    constructor(
        private readonly location: Location,
        private readonly cardService: CardService,
        private readonly loaderState: LoaderState,
        private readonly userService: UserService
    ) { }

    ngOnInit(): void {
        this.userService.getUsers()
        .subscribe((users: User[]) => {
            this.listOfUsers = users;
        });
    }

    isNotSelected(value: string): boolean {
        return this.listOfSelectedValue.indexOf(value) === -1;
    }

    /**
     * The selected date is changed
     * @param date selected date
     */
    onChangeDate(date: Date): void {
        this.selectedDate = date;
        this.getArhivedCards();
    }

    /**
     * Back button clicked
     */
     backClicked() {
        this.location.back();
    }

    /**
     * Get archived cards
     */
    private getArhivedCards(): void {
        this.loaderState.setLoader(true);

        combineLatest(
            this.cardService.getArchivedNPatientCards(this.selectedDate),
            this.cardService.getArchivedPatientCards(this.selectedDate)
        )
        .pipe(
            map((archivedCards: any) => archivedCards[0].concat(archivedCards[1])),
            finalize(() => this.loaderState.setLoader(false))
        )
        .subscribe((archivedCards: PatientCard[]|NoPatientCard[]) => this.cards = archivedCards);
    }
}
