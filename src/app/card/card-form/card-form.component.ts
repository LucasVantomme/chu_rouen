import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { take } from "rxjs/operators";
import { NzNotificationService } from "ng-zorro-antd";

import { CardState } from "../@core/card.state";
import { Book } from "../../book/@models/book.model";
import { BookState } from "../../book/@core/book.state";
import { PatientCard } from "../@models/patient-card.model";
import { CardService } from "../@core/card.service";
import { Card } from "../@models/card.model";
import { BookService } from "../../book/@core/book.service";
import { NoPatientCard } from "../@models/non-patient-card.model";
import { CriticiteService } from "../@core/criticite.service";
import { Crit } from "../@models/crit.models";
import { Secteur } from "../@models/secteur.model";
import { SecteurService } from "../@core/secteur.service";
import { FicheType } from "../@models/fiche-type.model";
import { FicheTypeService } from "../@core/fiche-type.service";
import { AuthState } from "src/app/@core/auth/auth.state";
import { User } from "src/app/auth/@model/user.model";

@Component({
    selector: "app-card-form",
    templateUrl: "./card-form.component.html",
    styleUrls: ["./card-form.component.scss"]
})
export class CardFormComponent implements OnInit {
    /**
     * Selected card type for the form
     */
    selectedCardType: string;
    /**
     * Patient card Form
     */
    patientCardForm: FormGroup;
    /**
     * No Patient card Form
     */
    noPatientCardForm: FormGroup;
    /**
     * Birth Date has Error
     */
    birthDateHasError: boolean = false;
    criticities: Crit[] = [];
    secteurs: Secteur[] = [];
    ficheTypes: FicheType[] = [];
    user: User;
    /**
     * Selected Book
     */
    private selectedBook: Book;
    private uploadedFile;
    private url;

    /**
     * Card form constructor
     * @param formBuilder Form Builder
     * @param location Location
     * @param cardState Card State
     * @param bookState Book state
     * @param cardService Card Service
     * @param notification Notification Service from Ant Design
     * @param bookService Book service
     */
    constructor(
        private readonly formBuilder: FormBuilder,
        private readonly location: Location,
        private readonly cardState: CardState,
        private readonly bookState: BookState,
        private readonly cardService: CardService,
        private readonly notification: NzNotificationService,
        private readonly bookService: BookService,
        private readonly critService: CriticiteService,
        private readonly secteurService: SecteurService,
        private readonly ficheTypeService: FicheTypeService,
        private readonly authState: AuthState
    ) { }

    ngOnInit() {
        this.cardState.getSelectedTypeCard()
        .subscribe((type: string) => this.selectedCardType = type);

        this.bookState.getSelectedBook()
        .pipe(take(1))
        .subscribe((book: Book) => this.selectedBook = book);

        this.critService.getCriticites()
        .subscribe((criticites: Crit[]) => this.criticities = criticites);

        this.secteurService.getSecteurs()
        .subscribe((secteurs: Secteur[]) => this.secteurs = secteurs);

        this.ficheTypeService.getFicheType()
        .subscribe((ficheTypes: FicheType[]) => this.ficheTypes = ficheTypes);

        this.authState.getUser()
        .subscribe((user: User) => this.user = user);

        this.patientCardForm = this.formBuilder.group({
            title: [null, [Validators.required]],
            patientName: [null, [Validators.required]],
            sector: [null, [Validators.required]],
            criticity: [null, [Validators.required]],
            bookId: [null, [Validators.required]],
            birthDate: [null, [Validators.required]],
            description: [null, [Validators.required]],
            conclusion: [null, [Validators.required]],
            creatorId: [null, [Validators.required]],
            archiveDate: [null, [Validators.required]]
        });

        this.noPatientCardForm = this.formBuilder.group({
            title: [null, [Validators.required]],
            sector: [null, [Validators.required]],
            criticity: [null, [Validators.required]],
            type: [null, [Validators.required]],
            free_text: [null, [Validators.required]],
            description: [null, [Validators.required]],
            conclusion: [null, [Validators.required]],
            creatorId: [null, [Validators.required]],
            archiveDate: [null, [Validators.required]]
        });
    }

    /**
     * Back button clicked
     */
    backClicked() {
        this.location.back();
    }

    onFileChanged(blah: any): void {
        // this.uploadedFile = blah.target.files[0];

        // const reader = new FileReader();
        // reader.readAsDataURL(blah.target.files[0]);
        // reader.onload = (event) => {
        //     this.url = reader.result;
        // };
    }

    addNewPatientCard(): void {
        // const uploadDate = new FormData();
        // uploadDate.append("file", this.uploadedFile, this.uploadedFile.name);

        const dateReg = /^\d{2}[/]\d{2}[/]\d{4}$/;
        this.birthDateHasError = (this.patientCardForm.get("birthDate").value.match(dateReg)) ? false : true;

        if (!this.birthDateHasError) {
            const parts = this.patientCardForm.get("birthDate").value.split("/");

            const newPatientCard = new PatientCard(
                this.patientCardForm.get("title").value,
                false,
                this.patientCardForm.get("patientName").value,
                this.patientCardForm.get("sector").value,
                this.patientCardForm.get("bookId").value,
                new Date(parts[2], parts[1] - 1, parts[0], 12, 0),
                (this.patientCardForm.get("description").value !== null && this.patientCardForm.get("description").value !== "") ?
                    this.patientCardForm.get("description").value :
                    "",
                (this.patientCardForm.get("conclusion").value !== null && this.patientCardForm.get("conclusion").value !== "") ?
                    this.patientCardForm.get("conclusion").value :
                    "",
                new Date().toString(),
                this.patientCardForm.get("criticity").value,
            );

            this.cardService.addPatientCard(newPatientCard, this.selectedBook, this.user)
            .subscribe((cardCreated: any) => {
                this.notification.success("Nouvelle fiche enregistrée", `La fiche  ${cardCreated.Titre} est bien enregistrée`);
                this.bookService.getBookById(this.selectedBook.id).subscribe((book: Book) => {
                    this.bookState.setSelectedBook(book);
                    this.backClicked();
                });
            });
        }
    }

    addNewNoPatientCard(): void {
        const newNoPatientCard = new NoPatientCard(
            this.noPatientCardForm.get("title").value,
            false,
            this.noPatientCardForm.get("type").value,
            this.noPatientCardForm.get("sector").value,
            (this.noPatientCardForm.get("free_text").value !== null && this.noPatientCardForm.get("free_text").value !== "") ?
                this.noPatientCardForm.get("free_text").value :
                "",
            (this.noPatientCardForm.get("description").value !== null && this.noPatientCardForm.get("description").value !== "") ?
                this.noPatientCardForm.get("description").value :
                "",
            (this.noPatientCardForm.get("conclusion").value !== null && this.noPatientCardForm.get("conclusion").value !== "") ?
                this.noPatientCardForm.get("conclusion").value :
                "",
            new Date().toString(),
            this.noPatientCardForm.get("criticity").value
        );

        this.cardService.addNoPatientCard(newNoPatientCard, this.selectedBook, this.user)
        .subscribe((cardCreated: any) => {
            this.notification.success("Nouvelle fiche enregistrée", `La fiche  ${cardCreated.Titre} est bien enregistrée`);
            this.bookService.getBookById(this.selectedBook.id).subscribe((book: Book) => {
                this.bookState.setSelectedBook(book);
                this.backClicked();
            });
        });
    }

}
