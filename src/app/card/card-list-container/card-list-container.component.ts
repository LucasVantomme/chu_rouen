import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { take } from "rxjs/operators";
import * as moment from "moment";

import { BookState } from "../../book/@core/book.state";
import { Book } from "../../book/@models/book.model";
import { Card } from "../@models/card.model";
import { PatientCard } from "../@models/patient-card.model";
import { NoPatientCard } from "../@models/non-patient-card.model";

@Component({
    selector: "app-card-list-container",
    templateUrl: "./card-list-container.component.html",
    styleUrls: ["./card-list-container.component.scss"]
})
export class CardListContainerComponent implements OnInit {
    /**
     * Selected Book
     */
    selectedBook: Book;
    /**
     * Cards of selected book
     */
    cards: Card[] = [];
    /**
     * All cards get from API
     */
    private allCards: Card[];
    /**
     * Include archived cards in the list
     */
    private includeArchivedCard: boolean = false;
    /**
     * Research
     */
    private research: string = "";

    /**
     * Card list container component constructor
     * @param bookState Book state
     * @param location Location
     */
    constructor(
        private readonly bookState: BookState,
        private readonly location: Location
    ) { }

    ngOnInit() {
        this.bookState.getSelectedBook()
        .pipe(
            take(1)
        )
        .subscribe((book: Book) =>  {
            this.selectedBook = book;
            this.allCards = book.patientCards.concat(book.noPatientCards);
            this.cards = this.allCards.filter((card: PatientCard|NoPatientCard) => card.isArchived !== true);
        });
    }

    /**
     * Back button clicked
     */
    backClicked() {
        this.location.back();
    }

    /**
     * Search card
     * @param search search input
     */
    searchCard(search: string): void {
        this.research = search;
        const cards = this.allCards.filter((card: any) => {
            console.log();
            return ((card.title && card.title.includes(search)) ||
            ((card.patientName && card.patientName.toUpperCase().includes(search.toUpperCase()))) ||
            ((card.description && card.description.toUpperCase().includes(search.toUpperCase()))) ||
            ((card.conclusion && card.conclusion.toUpperCase().includes(search.toUpperCase()))) ||
            ((card.archiveDate && moment(new Date(card.archiveDate)).format("LL").toUpperCase().includes(search))) ||
            ((card.type && card.type.toUpperCase().includes(search.toUpperCase()))) ||
            ((card.freeText && card.freeText.toUpperCase().includes(search.toUpperCase()))) ||
            (card.criticity && card.criticity.toUpperCase().includes(search.toUpperCase())) ||
            (card.sector && card.sector.toUpperCase().includes(search.toUpperCase())) ||
            (card.bookId && card.bookId.toString().toUpperCase().includes(search.toUpperCase())) ||
            (card.birthDate && moment(new Date(card.birthDate)).format("LL").toUpperCase().includes(search.toUpperCase())) ||
            (card.creationDate && moment(new Date(card.creationDate)).format("LL").toUpperCase().includes(search))) &&
            (this.includeArchivedCard || card.isArchived === false);
        }
        );
        this.cards = cards;
    }

    /**
     * Include archived cards if necessary
     * @param included include cards
     */
    includeArchivedCards(included: boolean): void {
        this.includeArchivedCard = !this.includeArchivedCard;
        this.searchCard(this.research);
    }
}

