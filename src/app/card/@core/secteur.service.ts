import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { finalize, map } from "rxjs/operators";

import { User } from "../../auth/@model/user.model";

import { LoaderState } from "../../@core/loader/loader.state";
import { Crit } from "../@models/crit.models";
import { Secteur } from "../@models/secteur.model";

/**
 * Secteur service
 */
@Injectable({
    providedIn: "root"
})
export class SecteurService {

    /**
     * Secteur service's constructor
     * @param http Http Client
     * @param loaderState Loader State
     */
    constructor(
        private readonly http: HttpClient,
        private readonly loaderState: LoaderState
    ) { }

    /**
     * Get Secteurs
     */
    getSecteurs(): Observable<Secteur[]> {
        this.loaderState.setLoader(true);
        return this.http.get("/api/secteurs")
        .pipe(map((secteurs: any[]) => secteurs.map((secteur: any) => new Secteur(secteur.Nom))),
        finalize(() => this.loaderState.setLoader(false)));
    }
}
