import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { finalize, map } from "rxjs/operators";

import { User } from "../../auth/@model/user.model";

import { LoaderState } from "../../@core/loader/loader.state";
import { Crit } from "../@models/crit.models";

/**
 * Criticite service
 */
@Injectable({
    providedIn: "root"
})
export class CriticiteService {

    /**
     * Criticite service's constructor
     * @param http Http Client
     * @param loaderState Loader State
     */
    constructor(
        private readonly http: HttpClient,
        private readonly loaderState: LoaderState
    ) { }

    /**
     * Get Criticites
     */
    getCriticites(): Observable<Crit[]> {
        this.loaderState.setLoader(true);
        return this.http.get("/api/criticites")
        .pipe(map((criticites: any[]) => criticites.map((crit: any) => new Crit(crit.Nom))),
        finalize(() => this.loaderState.setLoader(false)));
    }
}
