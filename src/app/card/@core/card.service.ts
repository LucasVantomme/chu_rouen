import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { finalize, map } from "rxjs/operators";

import { Card } from "../@models/card.model";
import { PatientCard } from "../@models/patient-card.model";
import { Book } from "../../book/@models/book.model";
import { NoPatientCard } from "../@models/non-patient-card.model";
import { LoaderState } from "src/app/@core/loader/loader.state";
import { DatePipe } from "@angular/common";
import { User } from "src/app/auth/@model/user.model";

/**
 * Card service
 */
@Injectable({
    providedIn: "root"
})
export class CardService {

    /**
     * Card service's constructor
     * @param http Http Client
     * @param loaderState Loader State
     */
    constructor(
        private readonly http: HttpClient,
        private readonly loaderState: LoaderState,
        private readonly datePipe: DatePipe
    ) { }

    /**
     * Add a new card
     * @param card Card to add
     * @param selectedBook Selected Book
     */
    addPatientCard(card: PatientCard, selectedBook: Book, user: User): Observable<any> {
        this.loaderState.setLoader(true);
        const cardToAdd = {
            Titre: card.title,
            criticite: card.criticity,
            DateCreation: new Date(),
            Secteur: card.sector,
            NomPatient: card.patientName,
            DateNaissance: card.birthDate,
            NumDossier: Number(card.bookId),
            Description: card.description,
            Conclusion: card.conclusion,
            DateArchive: card.archiveDate,
            Createur: {
                User_ID: user.id
            },
            Cahier: {
                id: selectedBook.id
            },
            isArchived: false
        };

        return this.http.post("/api/addfiche", cardToAdd)
        .pipe(map((newCard: any) => cardToAdd),
        finalize(() => this.loaderState.setLoader(false)));
    }

    /**
     * Add a new card
     * @param card Card to add
     */
     addNoPatientCard(card: NoPatientCard, selectedBook: Book, user: User): Observable<any> {
        this.loaderState.setLoader(true);
        const cardToAdd = {
            Titre: card.title,
            Criticite: card.criticity,
            DateCreation: new Date(),
            Secteur: card.sector,
            Type: card.type,
            TexteLibre: card.freeText,
            Description: card.description,
            Conclusion: card.conclusion,
            Createur: {
                User_ID: user.id
            },
            Cahier: {
                id: selectedBook.id
            },
            isArchived: false
        };

        return this.http.post("/api/addNfiche", cardToAdd)
        .pipe(map((newCard: any) => cardToAdd),
        finalize(() => this.loaderState.setLoader(false)));
    }

    /**
     * Archive a patient card
     * @param card Card
     */
    archivePatientCard(card: PatientCard): Observable<any> {
        this.loaderState.setLoader(true);
        const cardToAdd = {
            isArchived: true,
            DateArchive: new Date()
        };

        return this.http.put(`/api/updatefiche/${card.id}`, cardToAdd)
        .pipe(map((newCard: any) => cardToAdd),
        finalize(() => this.loaderState.setLoader(false)));
    }

    /**
     * Unarchive a patient card
     * @param card Card
     */
    unarchivePatientCard(card: any): Observable<any> {
        this.loaderState.setLoader(true);
        const cardToAdd = {
            isArchived: false,
            DateArchive: null
        };

        return this.http.put(`/api/updatefiche/${card.id}`, cardToAdd)
        .pipe(map((newCard: any) => cardToAdd),
        finalize(() => this.loaderState.setLoader(false)));
    }

    /**
     * update a patient card
     * @param card Card
     */
     updatePatientCard(card: PatientCard, selectedBook: Book): Observable<any> {
        this.loaderState.setLoader(true);
        const cardToAdd = {
            Titre: card.title,
            Criticite: card.criticity,
            Secteur: card.sector,
            NomPatient: card.patientName,
            NumDossier: Number(card.bookId),
            Description: card.description,
            Conclusion: card.conclusion,
            DateNaissance: card.birthDate,
        };

        return this.http.put(`/api/updatefiche/${card.id}`, cardToAdd)
        .pipe(map((newCard: any) => cardToAdd),
        finalize(() => this.loaderState.setLoader(false)));
    }

    /**
     * Archive a object  card
     * @param card Card
     */
    archiveObjectCard(card: NoPatientCard): Observable<any> {
        this.loaderState.setLoader(true);
        const cardToAdd = {
            isArchived: true,
            DateArchive: new Date()
        };

        return this.http.put(`/api/updateNfiche/${card.id}`, cardToAdd)
        .pipe(map((newCard: any) => cardToAdd),
        finalize(() => this.loaderState.setLoader(false)));
    }

    /**
     * Unarchive a object  card
     * @param card Card
     */
    unarchiveObjectCard(card: any): Observable<any> {
        this.loaderState.setLoader(true);
        const cardToAdd = {
            isArchived: false,
            DateArchive: null
        };

        return this.http.put(`/api/updateNfiche/${card.id}`, cardToAdd)
        .pipe(map((newCard: any) => cardToAdd),
        finalize(() => this.loaderState.setLoader(false)));
    }

    /**
     * Archive a object  card
     * @param card Card
     */
    updateObjectCard(card: NoPatientCard, selectedBook: Book): Observable<any> {
        this.loaderState.setLoader(true);
        const cardToAdd = {
            Titre: card.title,
            Criticite: card.criticity,
            Secteur: card.sector,
            Type: card.type,
            TexteLibre: card.freeText,
            Description: card.description,
            Conclusion: card.conclusion,
        };

        return this.http.put(`/api/updateNfiche/${card.id}`, cardToAdd)
        .pipe(map((newCard: any) => cardToAdd),
        finalize(() => this.loaderState.setLoader(false)));
    }

    /**
     * Get archived patient cards
     * @param date selected date
     */
    getArchivedPatientCards(date: Date): Observable<any> {
        const formatedDate = this.datePipe.transform(date, "yyyy-MM-dd");

        return this.http.get(`/api/fichepatientdate/${formatedDate}`)
        .pipe(map((newCard: any) => newCard));
    }

    /**
     * Get archived Npatient cards
     * @param date selected date
     */
     getArchivedNPatientCards(date: Date): Observable<any> {
        const formatedDate = this.datePipe.transform(date, "yyyy-MM-dd");

        return this.http.get(`/api/ficheNpatientdate/${formatedDate}`)
        .pipe(map((newCard: any) => newCard));
    }
}
