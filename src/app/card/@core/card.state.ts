import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { Card } from "../@models/card.model";

@Injectable({
    providedIn: "root"
})
export class CardState {
    /**
     * selected type card
     */
    private readonly selectedTypeCard = new BehaviorSubject<string>(null);
    /**
     * selected card
     */
     private readonly selectedCard = new BehaviorSubject<Card>(null);

    constructor() { }

    /**
     * Set the selected type card
     * @param type selected type card
     */
    setSelectedTypeCard(type: string): void {
        this.selectedTypeCard.next(type);
    }

    /**
     * Get Selected type card
     */
    getSelectedTypeCard(): Observable<string> {
        return this.selectedTypeCard;
    }

    /**
     * Set the selected card
     * @param card selected pe card
     */
     setSelectedCard(card: Card): void {
        this.selectedCard.next(card);
    }

    /**
     * Get Selected card
     */
     getSelectedCard(): Observable<Card> {
        return this.selectedCard;
    }
}
