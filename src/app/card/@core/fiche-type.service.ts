import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { finalize, map } from "rxjs/operators";

import { User } from "../../auth/@model/user.model";

import { LoaderState } from "../../@core/loader/loader.state";
import { Crit } from "../@models/crit.models";
import { Secteur } from "../@models/secteur.model";
import { FicheType } from "../@models/fiche-type.model";

/**
 * FicheType service
 */
@Injectable({
    providedIn: "root"
})
export class FicheTypeService {

    /**
     * FicheType service's constructor
     * @param http Http Client
     * @param loaderState Loader State
     */
    constructor(
        private readonly http: HttpClient,
        private readonly loaderState: LoaderState
    ) { }

    /**
     * Get FicheType
     */
    getFicheType(): Observable<Secteur[]> {
        this.loaderState.setLoader(true);
        return this.http.get("/api/fichestypes")
        .pipe(map((ficheTypes: any[]) => ficheTypes.map((ficheType: any) => new FicheType(ficheType.Nom))),
        finalize(() => this.loaderState.setLoader(false)));
    }
}
