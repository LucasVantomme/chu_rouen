import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { take } from "rxjs/operators";
import { NzNotificationService } from "ng-zorro-antd";

import { CardState } from "../@core/card.state";
import { PatientCard } from "../@models/patient-card.model";
import { NoPatientCard } from "../@models/non-patient-card.model";
import { BookState } from "../../book/@core/book.state";
import { Book } from "../../book/@models/book.model";
import { CardService } from "../@core/card.service";
import { BookService } from "../../book/@core/book.service";

@Component({
    selector: "app-card",
    templateUrl: "./card.component.html",
    styleUrls: ["./card.component.scss"]
})
export class CardComponent implements OnInit {
    /**
     * Patient Card Form
     */
    patientCardForm: FormGroup;
    /**
     * No Patient card Form
     */
    noPatientCardForm: FormGroup;
    /**
     * Selected card
     */
    selectedCard: PatientCard|NoPatientCard;
    /**
     * Birth Date has Error
     */
    birthDateHasError: boolean = false;
    /**
     * Selected book
     */
    private selectedBook: Book;

    /**
     * Card component constructor
     * @param formBuilder Form Builder
     * @param location Location
     * @param cardState Card State
     * @param bookState Book State
     * @param cardService Card Service
     * @param notification Notification Service
     * @param bookService Book Service
     */
    constructor(
        private readonly formBuilder: FormBuilder,
        private readonly location: Location,
        private readonly cardState: CardState,
        private readonly bookState: BookState,
        private readonly cardService: CardService,
        private readonly notification: NzNotificationService,
        private readonly bookService: BookService
    ) { }

    /**
     * Angular cycle onInit
     */
    ngOnInit(): void {
        this.bookState.getSelectedBook()
        .pipe(
            take(1)
        )
        .subscribe((book: Book) =>  {
            this.selectedBook = book;
            // this.allCards = book.patientCards.concat(book.noPatientCards);
        });

        this.cardState.getSelectedCard()
        .subscribe((card: PatientCard|NoPatientCard) => this.setForm(card));
    }

    /**
     * Back button clicked
     */
    backClicked() {
        this.location.back();
    }

    /**
     * Get other card
     * @param cardIndex Card index
     */
    otherCard(cardIndex: number): void {
        const allCards: any[] = this.selectedBook.patientCards.concat(this.selectedBook.noPatientCards);
        if (allCards.indexOf(this.selectedCard) + cardIndex >= 0) {
            if (allCards[allCards.indexOf(this.selectedCard) + cardIndex].isArchived) {
                if (cardIndex < 0) {
                    this.otherCard(cardIndex - 1);
                } else if (cardIndex > 0) {
                    this.otherCard(cardIndex + 1);
                }
            } else {
                this.cardState.setSelectedCard(allCards[allCards.indexOf(this.selectedCard) + cardIndex]);
            }
        }
    }

    /**
     * Get Index card
     */
    getIndexCard(cardIndex: number): boolean {
        const allCards: any[] = this.selectedBook.patientCards.concat(this.selectedBook.noPatientCards);
        if (allCards.indexOf(this.selectedCard) + cardIndex === -1 ||
            allCards.indexOf(this.selectedCard) + cardIndex === allCards.length) {
            return true;
        } else if (allCards[allCards.indexOf(this.selectedCard) + cardIndex].isArchived) {
            if (cardIndex < 0) {
                return this.getIndexCard(cardIndex - 1);
            } else if (cardIndex > 0) {
                return this.getIndexCard(cardIndex + 1);
            }
        } else {
            return false;
        }
    }

    updateCard(): void {
        if (this.patientCardForm) {
            this.updatePatientCard();
        } else if (this.noPatientCardForm) {
            this.updateNoPatientCard();
        }
    }

    unarchiveCard(): void {
        if (this.patientCardForm) {
            this.unarchivePatientCard();
        } else if (this.noPatientCardForm) {
            this.unarchiveNoPatientCard();
        }
    }

    /**
     * Set form
     * @param selectedCard Selected card
     */
    private setForm(selectedCard: any): void {
        this.selectedCard = selectedCard;
        if (selectedCard.patientName) {
            this.noPatientCardForm = null;
            this.patientCardForm = this.formBuilder.group({
                title: [selectedCard.title, [Validators.required]],
                patientName: [selectedCard.patientName, [Validators.required]],
                sector: [selectedCard.sector, [Validators.required]],
                criticity: [selectedCard.criticity, [Validators.required]],
                bookId: [selectedCard.bookId, [Validators.required]],
                birthDate: [new Date(selectedCard.birthDate).toLocaleDateString(), [Validators.required]],
                description: [selectedCard.description, [Validators.required]],
                conclusion: [selectedCard.conclusion, [Validators.required]],
                creatorId: [selectedCard.creatorId, [Validators.required]],
                archiveDate: [selectedCard.archiveDate, [Validators.required]]
            });
        } else {
            this.patientCardForm = null;
            this.noPatientCardForm = this.formBuilder.group({
                title: [selectedCard.title, [Validators.required]],
                sector: [selectedCard.sector, [Validators.required]],
                criticity: [selectedCard.criticity, [Validators.required]],
                type: [selectedCard.type, [Validators.required]],
                free_text: [selectedCard.freeText, [Validators.required]],
                description: [selectedCard.description, [Validators.required]],
                conclusion: [selectedCard.conclusion, [Validators.required]],
                creatorId: [selectedCard.creatorId, [Validators.required]],
                archiveDate: [selectedCard.archiveDate, [Validators.required]]
            });
        }
    }

    private updatePatientCard(): void {
        // const uploadDate = new FormData();
        // uploadDate.append("file", this.uploadedFile, this.uploadedFile.name);

        const dateReg = /^\d{2}[/]\d{2}[/]\d{4}$/;
        this.birthDateHasError = (this.patientCardForm.get("birthDate").value.match(dateReg)) ? false : true;
        if (!this.birthDateHasError) {
            const parts = this.patientCardForm.get("birthDate").value.split("/");

            const newPatientCard = new PatientCard(
                this.patientCardForm.get("title").value,
                false,
                this.patientCardForm.get("patientName").value,
                this.patientCardForm.get("sector").value,
                this.patientCardForm.get("bookId").value,
                new Date(parts[2], parts[1] - 1, parts[0], 12, 0),
                (this.patientCardForm.get("description").value !== null && this.patientCardForm.get("description").value !== "") ?
                    this.patientCardForm.get("description").value :
                    " ",
                (this.patientCardForm.get("conclusion").value !== null && this.patientCardForm.get("conclusion").value !== "") ?
                    this.patientCardForm.get("conclusion").value :
                    " ",
                this.selectedCard.creationDate,
                this.patientCardForm.get("criticity").value,
            );

            newPatientCard.id = this.selectedCard.id;

            this.cardService.updatePatientCard(newPatientCard, this.selectedBook)
            .subscribe((cardCreated: any) => {
                this.notification.info("Fiche mise à jour", `La fiche ${cardCreated.Titre} est bien mise à jour`);
                this.bookService.getBookById(this.selectedBook.id).subscribe((book: Book) => {
                    this.bookState.setSelectedBook(book);
                    this.backClicked();
                });
            });
        }
    }

    private updateNoPatientCard(): void {
        const newNoPatientCard = new NoPatientCard(
            this.noPatientCardForm.get("title").value,
            false,
            this.noPatientCardForm.get("type").value,
            this.noPatientCardForm.get("sector").value,
            (this.noPatientCardForm.get("free_text").value !== null && this.noPatientCardForm.get("free_text").value !== "") ?
                this.noPatientCardForm.get("free_text").value :
                " ",
            (this.noPatientCardForm.get("description").value !== null && this.noPatientCardForm.get("description").value !== "") ?
                this.noPatientCardForm.get("description").value :
                " ",
            (this.noPatientCardForm.get("conclusion").value !== null && this.noPatientCardForm.get("conclusion").value !== "") ?
                this.noPatientCardForm.get("conclusion").value :
                " ",
            new Date().toString(),
            this.noPatientCardForm.get("criticity").value
        );
        newNoPatientCard.id = this.selectedCard.id;

        this.cardService.updateObjectCard(newNoPatientCard, this.selectedBook)
        .subscribe((cardCreated: any) => {
            this.notification.info("Fiche mise à jour", `La fiche ${cardCreated.Titre} est bien mise à jour`);
            this.bookService.getBookById(this.selectedBook.id).subscribe((book: Book) => {
                this.bookState.setSelectedBook(book);
                this.backClicked();
            });
        });
    }

    private unarchivePatientCard(): void {
        this.cardService.unarchivePatientCard(this.selectedCard)
            .subscribe((cardCreated: any) => {
                this.notification.info("Fiche desarchivée", `La fiche ${this.selectedCard.title} est bien desarchivée`);
                this.bookService.getBookById(this.selectedBook.id).subscribe((book: Book) => {
                    this.bookState.setSelectedBook(book);
                    this.backClicked();
                });
            });

    }

    private unarchiveNoPatientCard(): void {
        this.cardService.unarchiveObjectCard(this.selectedCard)
            .subscribe((cardCreated: any) => {
                this.notification.info("Fiche mise à jour", `La fiche ${this.selectedCard.title} est desarchivée`);
                this.bookService.getBookById(this.selectedBook.id).subscribe((book: Book) => {
                    this.bookState.setSelectedBook(book);
                    this.backClicked();
                });
            });

    }
}
