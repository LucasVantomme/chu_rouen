import { LOCALE_ID, NgModule } from "@angular/core";
import { CommonModule, DatePipe } from "@angular/common";
import { registerLocaleData } from "@angular/common";
import fr from "@angular/common/locales/fr";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { NzTableModule } from "ng-zorro-antd/table";
import { NzButtonModule } from "ng-zorro-antd/button";
import { NzDropDownModule } from "ng-zorro-antd/dropdown";
import { NzInputModule } from "ng-zorro-antd/input";
import { NzSelectModule } from "ng-zorro-antd/select";
import { NzDatePickerModule } from "ng-zorro-antd/date-picker";
import { NzFormModule } from "ng-zorro-antd/form";
import { NzIconModule } from "ng-zorro-antd/icon";
import { NzCheckboxModule } from "ng-zorro-antd/checkbox";
import { NZ_I18N, fr_FR } from "ng-zorro-antd/i18n";

import { AppRoutingModule } from "../app-routing.module";
import { CardFormComponent } from "./card-form/card-form.component";
import { CardListComponent } from "./card-list/card-list.component";
import { CardListContainerComponent } from "./card-list-container/card-list-container.component";
import { CardReportComponent } from "./card-report/card-report.component";
import { CardComponent } from "./card/card.component";

registerLocaleData(fr);

@NgModule({
    declarations: [
        CardFormComponent,
        CardListComponent,
        CardListContainerComponent,
        CardReportComponent,
        CardComponent
    ],
    imports: [
        CommonModule,
        AppRoutingModule,
        BrowserModule,
        ReactiveFormsModule,
        FormsModule,
        NzTableModule,
        NzButtonModule,
        NzDropDownModule,
        NzInputModule,
        NzSelectModule,
        NzDatePickerModule,
        NzFormModule,
        NzIconModule,
        NzCheckboxModule
    ],
    providers   : [
        { provide: NZ_I18N, useValue: fr_FR },
        { provide: LOCALE_ID, useValue: "fr" },
        DatePipe
    ]
})
export class CardModule { }
