export class FicheType {
    name: string;

    constructor(name: string) {
        this.name = name;
    }
}
