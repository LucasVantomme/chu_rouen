/**
 * No Patient Card model
 */
export class NoPatientCard {
    /**
     * ID
     */
    id: number;
    /**
     * Card's title
     */
    title: string;
    /**
     * type
     */
    type: string;
    /**
     * sector
     */
    sector: string;
    /**
     * Criticity
     */
    criticity: string;
    /**
     * free_text id
     */
    freeText: string;
    /**
     * Description
     */
    description: string;
    /**
     * Conclusion
     */
    conclusion: string;
    /**
     * Creator
     */
    creator: any;
    /**
     * Archive date
     */
    archiveDate: string;
    /**
     * Card's creation date
     */
    creationDate: string;
    /**
     * Card is archived
     */
    isArchived: boolean;

    // tslint:disable-next-line:max-line-length
    constructor(title: string, isArchived: boolean, type: string, sector: string, freeText: string, description: string, conclusion: string, creationDate: string, criticity?: string, creator?: any, id?: number, archiveDate?: string) {
        this.title = title;
        this.isArchived = isArchived;
        this.type = type;
        this.sector = sector;
        this.criticity = criticity;
        this.freeText = freeText;
        this.conclusion = conclusion;
        this.description = description;
        this.creationDate = creationDate;
        this.creator = creator;
        this.id = id;
        this.archiveDate = archiveDate;
    }

    getType(): string {
        return "NoPatientCard";
    }
}
