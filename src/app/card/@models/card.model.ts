/**
 * Card model
 */
export class Card {
    /**
     * Card's title
     */
    title: string;
    /**
     * Card's creation date
     */
    creationDate: string;

    constructor(title: string, creationDate: string, type: string) {
        this.title = title;
        this.creationDate = creationDate;
    }
}
