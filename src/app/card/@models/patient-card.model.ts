/**
 * Patient Card model
 */
export class PatientCard {
    /**
     * ID
     */
    id: number;
    /**
     * Card's title
     */
    title: string;
    /**
     * patient name
     */
    patientName: string;
    /**
     * sector
     */
    sector: string;
    /**
     * Criticity
     */
    criticity: string;
    /**
     * Book id
     */
    bookId: number;
    /**
     * Birth patient date
     */
    birthDate: Date;
    /**
     * Description
     */
    description: string;
    /**
     * Conclusion
     */
    conclusion: string;
    /**
     * Creator
     */
    creator: any;
    /**
     * Archive date
     */
    archiveDate: string;
    /**
     * Card's creation date
     */
    creationDate: string;
    /**
     * Card is archived
     */
    isArchived: boolean;

    // tslint:disable-next-line:max-line-length
    constructor(title: string, isArchived: boolean, patientName: string, sector: string, bookId: number, birthDate: Date, description: string, conclusion: string, creationDate: string, criticity?: string, creator?: any, id?: number, archiveDate?: string) {
        this.title = title;
        this.isArchived = isArchived;
        this.patientName = patientName;
        this.sector = sector;
        this.criticity = criticity;
        this.bookId = bookId;
        this.birthDate = birthDate;
        this.conclusion = conclusion;
        this.description = description;
        this.creationDate = creationDate;
        this.creator = creator;
        this.id = id;
        this.archiveDate = archiveDate;
    }

    getType(): string {
        return "PatientCard";
    }
}
