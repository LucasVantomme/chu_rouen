import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { AuthComponent } from "./auth/auth.component";
import { BookFormComponent } from "./book/book-form/book-form.component";
import { BookListContainerComponent } from "./book/book-list-container/book-list-container.component";
import { CardFormComponent } from "./card/card-form/card-form.component";
import { CardListContainerComponent } from "./card/card-list-container/card-list-container.component";
import { CardReportComponent } from "./card/card-report/card-report.component";
import { CardComponent } from "./card/card/card.component";
import { HomeComponent } from "./home/home.component";
import { StatisticsComponent } from "./statistics/statistics.component";
import { UserListComponent } from "./administration/user-list/user-list.component";
import { UserFormComponent } from "./administration/user-form/user-form.component";
import { RoleListComponent } from "./administration/role-list/role-list.component";
import { RoleFormComponent } from "./administration/role-form/role-form.component";

import {
    AuthGuardService as AuthGuard
} from "./@core/auth/auth-guard.service";
import { SectorFormComponent } from "./administration/sector-form/sector-form.component";
import { CriticityFormComponent } from "./administration/criticity-form/criticity-form.component";
import { TypeFormComponent } from "./administration/type-form/type-form.component";
import { LaboFormComponent } from "./administration/labo-form/labo-form.component";

const routes: Routes = [
    { path: "books", component: BookListContainerComponent, canActivate: [AuthGuard] },
    { path: "books/new", component: BookFormComponent, canActivate: [AuthGuard] },
    { path: "card", component: CardComponent, canActivate: [AuthGuard] },
    { path: "cards", component: CardListContainerComponent, canActivate: [AuthGuard] },
    { path: "cards/new", component: CardFormComponent, canActivate: [AuthGuard] },
    { path: "cards/report", component: CardReportComponent, canActivate: [AuthGuard] },
    { path: "statistics", component: StatisticsComponent, canActivate: [AuthGuard] },
    { path: "admin/user", component: UserListComponent, canActivate: [AuthGuard] },
    { path: "admin/user/new", component: UserFormComponent, canActivate: [AuthGuard] },
    { path: "admin/role", component: RoleListComponent, canActivate: [AuthGuard] },
    { path: "admin/role/new", component: RoleFormComponent, canActivate: [AuthGuard] },
    { path: "sector/new", component: SectorFormComponent, canActivate: [AuthGuard] },
    { path: "criticity/new", component: CriticityFormComponent, canActivate: [AuthGuard] },
    { path: "type/new", component: TypeFormComponent, canActivate: [AuthGuard] },
    { path: "labo/new", component: LaboFormComponent, canActivate: [AuthGuard] },
    { path: "", component: HomeComponent, canActivate: [AuthGuard] },
    { path: "login", component: AuthComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
