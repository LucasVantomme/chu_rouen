import { Role } from "./role.model";

/**
 * User model
 */
export class User {
    id: number;
    prenom: string;
    nom: string;
    labo: string;
    fullName: string;
    roles: Role[];
    idDelete: number;
    lastConnection: string;

    constructor(user: {id: number, prenom: string, nom: string, labo: string, roles: Role[], idDelete: number, lastConnection?: string}) {
        this.id = user.id;
        this.prenom = user.prenom;
        this.nom = user.nom;
        this.labo = user.labo;
        this.roles = user.roles;
        this.fullName = user.prenom + " " + user.nom;
        this.idDelete = user.idDelete;
        this.lastConnection = user.lastConnection;
    }
}
