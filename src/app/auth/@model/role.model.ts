/**
 * Role model
 */
 export class Role {
    id: number;
    titre: string;

    constructor(role: {id: number, titre: string}) {
        this.id = role.id;
        this.titre = role.titre;
    }
}
