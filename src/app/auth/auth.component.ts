import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Location } from "@angular/common";

import { AuthService } from "../@core/auth/auth.service";
import { AuthState } from "../@core/auth/auth.state";
import { User } from "./@model/user.model";
import { Router } from "@angular/router";
import { HttpErrorResponse } from "@angular/common/http";

@Component({
    selector: "app-auth",
    templateUrl: "./auth.component.html",
    styleUrls: ["./auth.component.scss"]
})
export class AuthComponent implements OnInit {
    validateForm: FormGroup;
    error: boolean = false;
    errorMessage: string;

    constructor(
        private readonly formBuilder: FormBuilder,
        private readonly authService: AuthService,
        private readonly authState: AuthState,
        private readonly router: Router,
    ) { }

    ngOnInit() {
        this.validateForm = this.formBuilder.group({
            userName: [null, [Validators.required]],
            password: [null, [Validators.required]]
        });
    }

    submitForm(): void {
        // tslint:disable-next-line:forin
        for (const i in this.validateForm.controls) {
            this.validateForm.controls[i].markAsDirty();
            this.validateForm.controls[i].updateValueAndValidity();
        }
        this.authService.login(this.validateForm.get("userName").value, this.validateForm.get("password").value)
        .subscribe(
            (connectedUser: User) => {
                this.error = false;
                this.authState.setUser(connectedUser);
                this.router.navigate(["books"]);
            }, (error: any) => {
                this.errorMessage = error;
                this.error = true;
            }
        );
    }
}
