import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Observable, throwError } from "rxjs";
import { catchError, finalize, map } from "rxjs/operators";
import { User } from "src/app/auth/@model/user.model";

import { LoaderState } from "../loader/loader.state";
import { AuthState } from "./auth.state";

@Injectable({
    providedIn: "root"
})
export class AuthService {
    isLogged: boolean = false;

    /**
     * Book service's constructor
     * @param http Http Client
     * @param loaderState Loader State
     */
     constructor(
        private readonly http: HttpClient,
        private readonly loaderState: LoaderState,
        private readonly authState: AuthState,
        public router: Router
    ) { }

    isAuthenticated(): boolean {
        return this.isLogged;
    }

    login(id: string, password: string): Observable<User> {
        this.loaderState.setLoader(true);
        return this.http.get(`/api/utilisateur/${id}/${password}`)
        .pipe(
            map((user: any) => {
                this.isLogged = true;
                return new User({
                    id: user[0].User_ID,
                    prenom: user[0].Prenom,
                    nom: user[0].Nom,
                    labo: user[0].Labo,
                    roles: user[0].Roles,
                    idDelete: user[0].id,
                    lastConnection: user[0].last_connexion
                });
            }),
            catchError((error) => {
                if (error.status === 404) {
                    return throwError("Numéro d'utilisateur ou mot de passe invalide.");
                } else if (error.status === 401) {
                    return throwError("Utilisateur desactivé. Veuillez contacter l'administrateur.");
                }
            }),
        finalize(() => this.loaderState.setLoader(false)));
    }

    logout(): void {
        this.isLogged = null;
        this.authState.setUser(null);
        this.router.navigate(["login"]);
    }
}
