import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { User } from "src/app/auth/@model/user.model";

@Injectable({
    providedIn: "root"
})
export class AuthState {
    private readonly user: BehaviorSubject<User> = new BehaviorSubject<User>(null);

    constructor() { }

    /**
     * Set the user
     * @param user User
     */
    setUser(user: User): void {
        this.user.next(user);
    }

    /**
     * Get user
     */
    getUser(): Observable<User> {
        return this.user;
    }
}
