import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";

@Injectable({
    providedIn: "root"
})
export class LoaderState {
    /**
     * Define the loader of app
     */
    private readonly loader: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    constructor() { }

    /**
     * Set the loader
     * @param isLoading loader
     */
    setLoader(isLoading: boolean): void {
        this.loader.next(isLoading);
    }

    /**
     * Get loader
     */
    getLoader(): Observable<boolean> {
        return this.loader;
    }
}
