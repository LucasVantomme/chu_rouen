import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from "@angular/core";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { AuthService } from "./@core/auth/auth.service";
import { AuthState } from "./@core/auth/auth.state";
import { LoaderState } from "./@core/loader/loader.state";
import { Role } from "./auth/@model/role.model";
import { User } from "./auth/@model/user.model";

@Component({
    selector: "app-root",
    templateUrl: "./app.component.html",
    styleUrls: ["./app.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit, OnDestroy {
    /**
     * App's title
     */
    title = "ASPIC";
    /**
     * Is loading
     */
    isLoading: boolean;
    /**
     * Connecter User
     */
    user: User;
    /**
     * Follow all subscription
     */
    private readonly subscriber: Subject<boolean> = new Subject<boolean>();

    /**
     * App component's constructor
     * @param loaderState Loader State
     */
    constructor(
        private readonly loaderState: LoaderState,
        private readonly cd: ChangeDetectorRef,
        private readonly authService: AuthService,
        private readonly authState: AuthState
    ) {}

    /**
     * Angular cycle OnInit
     */
    ngOnInit(): void {
        this.loaderState.getLoader()
        .pipe((takeUntil(this.subscriber)))
        .subscribe((isLoading) => {
            this.isLoading = isLoading;
            this.cd.detectChanges();
        });

        this.authState.getUser()
        .pipe((takeUntil(this.subscriber)))
        .subscribe((user: User) => {
            this.user = user;
            this.cd.markForCheck();
        });
    }

    /**
     * Angular Cycle OnDestroy
     */
    ngOnDestroy(): void {
        this.subscriber.next(true);
        this.subscriber.complete();
    }

    logout(): void {
        this.authService.logout();
    }

    isAdmin(): boolean {
        let isAdministrateur: boolean = false;

        this.user.roles.forEach((role: any) => {
            if (role.Titre === "Administrateur") {
                isAdministrateur = true;
            }
        });

        return isAdministrateur;
    }
}
