import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NzNotificationService } from "ng-zorro-antd";
import { Router } from "@angular/router";

import { Repository } from "../@model/repository.model";
import { RepositoryService } from "../@core/repository.service";

@Component({
    selector: "app-criticity-form",
    templateUrl: "./criticity-form.component.html",
    styleUrls: ["./criticity-form.component.scss"]
})
export class CriticityFormComponent implements OnInit {
    /**
     * criticity Form
     */
     criticityForm: FormGroup;

    /**
     * Constructor book form component
     * @param formBuilder Form Builder
     * @param location Location
     * @param notification Notification Service from Ant Design
     * @param router Router
     */
    constructor(
        private readonly formBuilder: FormBuilder,
        private readonly location: Location,
        private readonly notification: NzNotificationService,
        private readonly router: Router,
        private readonly repoService: RepositoryService
    ) { }

    /**
     * Angular Cycle OnInit
     */
    ngOnInit() {
        this.criticityForm = this.formBuilder.group({
            name: [null, [Validators.required]]
        });
    }

    /**
     * Back button clicked
     */
    backClicked() {
        this.location.back();
    }

    /**
     * Add new criticity
     */
    addNewCriticity(): void {
        if (this.criticityForm.get("name").value) {

            const criticity = {
                Nom: this.criticityForm.get("name").value
            };

            this.repoService.addCriticity(criticity)
            .subscribe((criticityCreated: Repository) => {
                this.notification.success("Nouvelle criticité enregistrée", `Le criticité ${criticity.Nom} est bien enregistrée`);
                this.router.navigate(["/"]);
            });
        } else {
            console.log("NOPEEEEEEEEEE");
        }
    }
}
