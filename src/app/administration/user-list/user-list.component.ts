import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { UserService } from "../@core/user.service";
import { User } from "src/app/auth/@model/user.model";
import { NzNotificationService } from "ng-zorro-antd";

@Component({
    selector: "app-user-list",
    templateUrl: "./user-list.component.html",
    styleUrls: ["./user-list.component.scss"]
})
export class UserListComponent implements OnInit {
    users: User[] = [];
    allUsers: User[] = [];
    /**
     * Search input
     */
    search: string = "";

    /**
     * @param location Location
     */
    constructor(
        private readonly location: Location,
        private readonly userService: UserService,
        private readonly notification: NzNotificationService
    ) { }

    /**
     * Angular Cycle OnInit
     */
    ngOnInit() {
        this.getUsers();
    }

    /**
     * Back button clicked
     */
    backClicked() {
        this.location.back();
    }

    /**
     * Search userx
     */
    searchUser(): void {
        const users = this.allUsers.filter((user: User) =>
            ((user.prenom && user.prenom.toUpperCase().includes(this.search.toUpperCase())) ||
            ((user.nom && user.nom.toUpperCase().includes(this.search.toUpperCase()))) ||
            ((user.labo && user.labo.toUpperCase().includes(this.search.toUpperCase())))
        ));
        this.users = users;
    }

    /**
     * Delete a user
     * @param user user
     */
     deleteUser(user: User): void {
        this.userService.deleteUser(user)
        .subscribe((deletedUser: User) => {
            this.getUsers();
            this.notification.success("Rôle supprimé", `La rôle ${deletedUser.fullName} est bien supprimé`);
        }, () => this.notification.error("Rôle non-supprimé",
            `La rôle ${user.fullName} n'est pas supprimé, un utilisateur possède ce rôle`));
    }

    private getUsers(): void {
        this.userService.getUsers()
        .subscribe((users: User[]) => {
            this.users = users;
            this.allUsers = users;
        });
    }
}
