import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { Role } from "src/app/auth/@model/role.model";
import { RoleService } from "../@core/role.service";
import { NzNotificationService } from "ng-zorro-antd";

@Component({
    selector: "app-role-list",
    templateUrl: "./role-list.component.html",
    styleUrls: ["./role-list.component.scss"]
})
export class RoleListComponent implements OnInit {
    roles: Role[] = [];
    allRoles: Role[] = [];
    /**
     * Search input
     */
    search: string = "";

    /**
     * @param location Location
     */
    constructor(
        private readonly location: Location,
        private readonly roleService: RoleService,
        private readonly notification: NzNotificationService
    ) { }

    /**
     * Angular Cycle OnInit
     */
    ngOnInit() {
        this.getRoles();
    }

    /**
     * Back button clicked
     */
    backClicked() {
        this.location.back();
    }

    /**
     * Search role
     */
    searchRole(): void {
        const roles = this.allRoles.filter((role: Role) =>
            ((role.titre && role.titre.toUpperCase().includes(this.search.toUpperCase()))
        ));
        this.roles = roles;
    }

    /**
     * Delete a role
     * @param role role
     */
    deleteRole(role: Role): void {
        this.roleService.deleteRole(role)
        .subscribe((deletedRole: Role) => {
            this.getRoles();
            this.notification.success("Rôle supprimé", `La rôle ${deletedRole.titre} est bien supprimé`);
        }, () => this.notification.error("Rôle non-supprimé", `La rôle ${role.titre} n'est pas supprimé, un utilisateur possède ce rôle`));
    }

    private getRoles(): void {
        this.roleService.getRoles()
        .subscribe((roles: Role[]) => {
            this.roles = roles;
            this.allRoles = roles;
        });
    }
}
