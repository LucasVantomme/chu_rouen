import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { finalize, map } from "rxjs/operators";

import { LoaderState } from "../../@core/loader/loader.state";
import { Role } from "src/app/auth/@model/role.model";

/**
 * Role service
 */
@Injectable({
    providedIn: "root"
})
export class RoleService {

    /**
     * Book service's constructor
     * @param http Http Client
     * @param loaderState Loader State
     */
    constructor(
        private readonly http: HttpClient,
        private readonly loaderState: LoaderState
    ) { }

    /**
     * Get roles
     */
    getRoles(): Observable<Role[]> {
        this.loaderState.setLoader(true);
        return this.http.get("/api/roles")
        .pipe(map((roles: any[]) => roles.map((role: any) => new Role({
            id: role.id,
            titre: role.Titre
        }))),
        finalize(() => this.loaderState.setLoader(false)));
    }

    /**
     * Add role
     * @param role role
     */
    addRole(role: any): Observable<Role> {
        this.loaderState.setLoader(true);
        return this.http.post("/api/addrole", role)
        .pipe(map((roleCreated: any) => new Role({
            id: roleCreated.id,
            titre: roleCreated.Titre
        })),
        finalize(() => this.loaderState.setLoader(false)));
    }

    /**
     * Delete role
     * @param role role
     */
    deleteRole(role: Role): Observable<Role> {
        this.loaderState.setLoader(true);
        return this.http.delete(`/api/deleterole/${role.id}`)
        .pipe(map((deletedRole: any) => role),
        finalize(() => this.loaderState.setLoader(false)));
    }
}
