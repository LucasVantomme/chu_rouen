import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { finalize, map } from "rxjs/operators";

import { User } from "../../auth/@model/user.model";

import { LoaderState } from "../../@core/loader/loader.state";

/**
 * User service
 */
@Injectable({
    providedIn: "root"
})
export class UserService {

    /**
     * Book service's constructor
     * @param http Http Client
     * @param loaderState Loader State
     */
    constructor(
        private readonly http: HttpClient,
        private readonly loaderState: LoaderState
    ) { }

    /**
     * Get users
     */
    getUsers(): Observable<User[]> {
        this.loaderState.setLoader(true);
        return this.http.get("/api/utilisateurs")
        .pipe(map((users: any[]) => users.map((user: any) => new User({
            id: user.User_ID,
            prenom: user.Prenom,
            nom: user.Nom,
            labo: user.Labo,
            roles: user.Roles,
            idDelete: user.id
        }))),
        finalize(() => this.loaderState.setLoader(false)));
    }

    /**
     * Add user
     * @param user user
     */
    addUser(user: any): Observable<User> {
        this.loaderState.setLoader(true);
        return this.http.post("/api/addutilisateur", user)
        .pipe(map((userCreated: any) => new User({
            id: userCreated.User_ID,
            prenom: userCreated.Prenom,
            nom: userCreated.Nom,
            labo: userCreated.Labo,
            roles: user.Roles,
            idDelete: user.id
        })),
        finalize(() => this.loaderState.setLoader(false)));
    }

    /**
     * Delete user
     * @param user user
     */
     deleteUser(user: User): Observable<User> {
        this.loaderState.setLoader(true);
        return this.http.delete(`/api/deleteutilisateur/${user.idDelete}`)
        .pipe(map((deletedUser: any) => user),
        finalize(() => this.loaderState.setLoader(false)));
    }
}
