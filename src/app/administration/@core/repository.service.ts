import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { finalize, map } from "rxjs/operators";

import { LoaderState } from "../../@core/loader/loader.state";
import { Repository } from "../@model/repository.model";

/**
 * Repository service
 */
@Injectable({
    providedIn: "root"
})
export class RepositoryService {

    /**
     * Book service's constructor
     * @param http Http Client
     * @param loaderState Loader State
     */
    constructor(
        private readonly http: HttpClient,
        private readonly loaderState: LoaderState
    ) { }

    /**
     * Add sector
     * @param sector sector
     */
    addSector(sector: any): Observable<Repository> {
        this.loaderState.setLoader(true);
        return this.http.post("/api/addsecteur", sector)
        .pipe(map((sectorCreated: any) => new Repository({
            id: sectorCreated.id,
            name: sectorCreated.Nom
        })),
        finalize(() => this.loaderState.setLoader(false)));
    }

    /**
     * Add criticity
     * @param criticity criticity
     */
     addCriticity(criticity: any): Observable<Repository> {
        this.loaderState.setLoader(true);
        return this.http.post("/api/addcriticite", criticity)
        .pipe(map((criticityCreated: any) => new Repository({
            id: criticityCreated.id,
            name: criticityCreated.Nom
        })),
        finalize(() => this.loaderState.setLoader(false)));
    }

    /**
     * Add type
     * @param type type
     */
    addType(type: any): Observable<Repository> {
        this.loaderState.setLoader(true);
        return this.http.post("/api/addfichetype", type)
        .pipe(map((typeCreated: any) => new Repository({
            id: typeCreated.id,
            name: typeCreated.Nom
        })),
        finalize(() => this.loaderState.setLoader(false)));
    }

    /**
     * Add labo
     * @param labo labo
     */
    addLabo(labo: any): Observable<Repository> {
        this.loaderState.setLoader(true);
        return this.http.post("/api/addlabo", labo)
        .pipe(map((laboCreated: any) => new Repository({
            id: laboCreated.id,
            name: laboCreated.Nom
        })),
        finalize(() => this.loaderState.setLoader(false)));
    }
}
