import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { finalize, map } from "rxjs/operators";

import { User } from "../../auth/@model/user.model";

import { LoaderState } from "../../@core/loader/loader.state";
import { Labo } from "../@model/laboratoy.model";

/**
 * Labo service
 */
@Injectable({
    providedIn: "root"
})
export class LaboService {

    /**
     * Labo service's constructor
     * @param http Http Client
     * @param loaderState Loader State
     */
    constructor(
        private readonly http: HttpClient,
        private readonly loaderState: LoaderState
    ) { }

    /**
     * Get labo
     */
    getLabos(): Observable<Labo[]> {
        this.loaderState.setLoader(true);
        return this.http.get("/api/labos")
        .pipe(map((labos: any[]) => labos.map((labo: any) => new Labo(labo.Nom))),
        finalize(() => this.loaderState.setLoader(false)));
    }
}
