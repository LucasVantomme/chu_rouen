import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NzNotificationService } from "ng-zorro-antd";
import { Router } from "@angular/router";

import { Repository } from "../@model/repository.model";
import { RepositoryService } from "../@core/repository.service";

@Component({
    selector: "app-labo-form",
    templateUrl: "./labo-form.component.html",
    styleUrls: ["./labo-form.component.scss"]
})
export class LaboFormComponent implements OnInit {
    /**
     * labo Form
     */
     laboForm: FormGroup;

    /**
     * Constructor book form component
     * @param formBuilder Form Builder
     * @param location Location
     * @param notification Notification Service from Ant Design
     * @param router Router
     */
    constructor(
        private readonly formBuilder: FormBuilder,
        private readonly location: Location,
        private readonly notification: NzNotificationService,
        private readonly router: Router,
        private readonly repoService: RepositoryService
    ) { }

    /**
     * Angular Cycle OnInit
     */
    ngOnInit() {
        this.laboForm = this.formBuilder.group({
            name: [null, [Validators.required]]
        });
    }

    /**
     * Back button clicked
     */
    backClicked() {
        this.location.back();
    }

    /**
     * Add new labo
     */
    addNewLabo(): void {
        if (this.laboForm.get("name").value) {

            const labo = {
                Nom: this.laboForm.get("name").value
            };

            this.repoService.addLabo(labo)
            .subscribe((laboCreated: Repository) => {
                this.notification.success("Nouveau laboratoire enregistré", `Le laboratoire ${labo.Nom} est bien enregistré`);
                this.router.navigate(["/"]);
            });
        } else {
            console.log("NOPEEEEEEEEEE");
        }
    }
}
