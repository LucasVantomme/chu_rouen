import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NzNotificationService } from "ng-zorro-antd";
import { Router } from "@angular/router";

import { UserService } from "../@core/user.service";
import { User } from "src/app/auth/@model/user.model";
import { LaboService } from "../@core/labo.service";
import { Labo } from "../@model/laboratoy.model";
import { RoleService } from "../@core/role.service";
import { Role } from "src/app/auth/@model/role.model";

@Component({
    selector: "app-role-form",
    templateUrl: "./role-form.component.html",
    styleUrls: ["./role-form.component.scss"]
})
export class RoleFormComponent implements OnInit {
    /**
     * role Form
     */
    roleForm: FormGroup;

    /**
     * Constructor book form component
     * @param formBuilder Form Builder
     * @param location Location
     * @param notification Notification Service from Ant Design
     * @param router Router
     */
    constructor(
        private readonly formBuilder: FormBuilder,
        private readonly location: Location,
        private readonly userService: UserService,
        private readonly notification: NzNotificationService,
        private readonly router: Router,
        private readonly laboService: LaboService,
        private readonly roleService: RoleService
    ) { }

    /**
     * Angular Cycle OnInit
     */
    ngOnInit() {
        this.roleForm = this.formBuilder.group({
            titre: [null, [Validators.required]]
        });
    }

    /**
     * Back button clicked
     */
    backClicked() {
        this.location.back();
    }

    /**
     * Add new role
     */
    addNewRole(): void {
        if (this.roleForm.get("titre").value) {

            const role = {
                Titre: this.roleForm.get("titre").value
            };

            this.roleService.addRole(role)
            .subscribe((roleCreated: Role) => {
                this.notification.success("Nouveau rôle enregistré", `Le rôle ${role.Titre} est bien enregistré`);
                this.router.navigate(["/admin/role"]);
            });
        } else {
            console.log("NOPEEEEEEEEEE");
        }
    }
}
