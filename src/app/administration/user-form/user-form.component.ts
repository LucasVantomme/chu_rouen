import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NzNotificationService } from "ng-zorro-antd";
import { Router } from "@angular/router";

import { UserService } from "../@core/user.service";
import { User } from "src/app/auth/@model/user.model";
import { LaboService } from "../@core/labo.service";
import { Labo } from "../@model/laboratoy.model";
import { RoleService } from "../@core/role.service";
import { Role } from "src/app/auth/@model/role.model";

@Component({
    selector: "app-user-form",
    templateUrl: "./user-form.component.html",
    styleUrls: ["./user-form.component.scss"]
})
export class UserFormComponent implements OnInit {
    /**
     * user Form
     */
    userForm: FormGroup;
    labos: Labo[] = [];
    roles: Role[] = [];

    /**
     * Constructor book form component
     * @param formBuilder Form Builder
     * @param location Location
     * @param notification Notification Service from Ant Design
     * @param router Router
     */
    constructor(
        private readonly formBuilder: FormBuilder,
        private readonly location: Location,
        private readonly userService: UserService,
        private readonly notification: NzNotificationService,
        private readonly router: Router,
        private readonly laboService: LaboService,
        private readonly roleService: RoleService
    ) { }

    /**
     * Angular Cycle OnInit
     */
    ngOnInit() {
        this.laboService.getLabos()
        .subscribe((labos: Labo[]) => this.labos = labos);

        this.userForm = this.formBuilder.group({
            prenom: [null, [Validators.required]],
            nom: [null, [Validators.required]],
            userID: [null, [Validators.required]],
            password: [null, [Validators.required]],
            groupe: [null, [Validators.required]],
            laboratoire: [null, [Validators.required]],
        });

        this.roleService.getRoles()
        .subscribe((roles: Role[]) => {
            this.roles = roles;
        });
    }

    /**
     * Back button clicked
     */
    backClicked() {
        this.location.back();
    }

    /**
     * Add new user
     */
    addNewUser(): void {
        if (this.userForm.get("prenom").value && this.userForm.get("nom").value && this.userForm.get("userID").value &&
            this.userForm.get("password").value && this.userForm.get("laboratoire").value && this.userForm.get("groupe").value) {

            const roles = [];
            this.userForm.get("groupe").value.forEach((role: any) => roles.push({id: role}));

            const user = {
                Prenom: this.userForm.get("prenom").value,
                Nom: this.userForm.get("nom").value,
                User_ID: this.userForm.get("userID").value,
                Password: this.userForm.get("password").value,
                Labo: this.userForm.get("laboratoire").value,
                Roles: roles,
                IsActive: true
            };

            this.userService.addUser(user)
            .subscribe((userCreated: User) => {
                this.notification.success("Nouvel utilisateur enregistré", `L'utilisateur ${user.Prenom} est bien enregistré`);
                this.router.navigate(["/admin/user"]);
            });
        } else {
            console.log("NOPEEEEEEEEEE");
        }
    }
}
