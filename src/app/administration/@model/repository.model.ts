/**
 * Repo model
 */
export class Repository {
    id: number;
    name: string;

    constructor(repo: {id: number, name: string}) {
        this.name = repo.name;
        this.id = repo.id;
    }
}
