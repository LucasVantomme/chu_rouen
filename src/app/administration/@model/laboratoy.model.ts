/**
 * Labo model
 */
 export class Labo {
    name: string;

    constructor(name: string) {
        this.name = name;
    }
}
