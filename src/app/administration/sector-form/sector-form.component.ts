import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NzNotificationService } from "ng-zorro-antd";
import { Router } from "@angular/router";

import { Repository } from "../@model/repository.model";
import { RepositoryService } from "../@core/repository.service";

@Component({
    selector: "app-sector-form",
    templateUrl: "./sector-form.component.html",
    styleUrls: ["./sector-form.component.scss"]
})
export class SectorFormComponent implements OnInit {
    /**
     * sector Form
     */
    sectorForm: FormGroup;

    /**
     * Constructor book form component
     * @param formBuilder Form Builder
     * @param location Location
     * @param notification Notification Service from Ant Design
     * @param router Router
     */
    constructor(
        private readonly formBuilder: FormBuilder,
        private readonly location: Location,
        private readonly notification: NzNotificationService,
        private readonly router: Router,
        private readonly repoService: RepositoryService
    ) { }

    /**
     * Angular Cycle OnInit
     */
    ngOnInit() {
        this.sectorForm = this.formBuilder.group({
            name: [null, [Validators.required]]
        });
    }

    /**
     * Back button clicked
     */
    backClicked() {
        this.location.back();
    }

    /**
     * Add new sector
     */
    addNewSector(): void {
        if (this.sectorForm.get("name").value) {

            const sector = {
                Nom: this.sectorForm.get("name").value
            };

            this.repoService.addSector(sector)
            .subscribe((sectorCreated: Repository) => {
                this.notification.success("Nouveau secteur enregistré", `Le secteur ${sector.Nom} est bien enregistré`);
                this.router.navigate(["/"]);
            });
        } else {
            console.log("NOPEEEEEEEEEE");
        }
    }
}
