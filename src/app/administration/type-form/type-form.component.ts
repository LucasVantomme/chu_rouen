import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NzNotificationService } from "ng-zorro-antd";
import { Router } from "@angular/router";

import { Repository } from "../@model/repository.model";
import { RepositoryService } from "../@core/repository.service";

@Component({
    selector: "app-type-form",
    templateUrl: "./type-form.component.html",
    styleUrls: ["./type-form.component.scss"]
})
export class TypeFormComponent implements OnInit {
    /**
     * type Form
     */
     typeForm: FormGroup;

    /**
     * Constructor book form component
     * @param formBuilder Form Builder
     * @param location Location
     * @param notification Notification Service from Ant Design
     * @param router Router
     */
    constructor(
        private readonly formBuilder: FormBuilder,
        private readonly location: Location,
        private readonly notification: NzNotificationService,
        private readonly router: Router,
        private readonly repoService: RepositoryService
    ) { }

    /**
     * Angular Cycle OnInit
     */
    ngOnInit() {
        this.typeForm = this.formBuilder.group({
            name: [null, [Validators.required]]
        });
    }

    /**
     * Back button clicked
     */
    backClicked() {
        this.location.back();
    }

    /**
     * Add new type
     */
    addNewType(): void {
        if (this.typeForm.get("name").value) {

            const type = {
                Nom: this.typeForm.get("name").value
            };

            this.repoService.addType(type)
            .subscribe((typereated: Repository) => {
                this.notification.success("Nouveau type enregistrée", `Le type ${type.Nom} est bien enregistré`);
                this.router.navigate(["/"]);
            });
        } else {
            console.log("NOPEEEEEEEEEE");
        }
    }
}
